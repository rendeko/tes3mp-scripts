## jrpAnim (for 0.7-alpha)
-	GUI for playing animations, with planned sitting/laying and idle animation support.
-	Under GPLv3

### Installation
-	Add jrpAnim.lua to scripts/custom/
-	Add to customScripts.lua:
		`require("custom.jrpAnim")`

### To-do
-   Idle animations (play random anim at random intervals every 2? minutes)
-	Chair support maybe? (use chair anim from mod)
