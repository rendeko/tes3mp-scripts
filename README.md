 A collection of TES3MP scripts by myself, including a fork directory of some other scripts I use on JRP Roleplay.

### Features
-	**antiMainQuest** - Disables accessing some main quest places
-	**balmoraFixMe** - Adds command to teleport to Balmora, for really stuck situations
-	**easyFind** - Simple GUI to print every players location
-	**factionCommands** - Simple menu to leave/unexpell yourself from factions
-	**jrpAnim** - Simple GUI to play player animations without commands
-	**jrpChat** - Rewrite of the chat system based around roleplay, with language and nickname support
-	**jrpCompanions** - Working, realistically usable companion/follower system (highly experimental)
-	**jrpRolls** - d100 rolling system with player-set buffs and debuffs
-	**jrpShop** - Customizable GUI for players to modify their character and buy clothing
-	**jrpStatus** - Players can set in-game viewable biographies, appearance descriptions, and other character sheet esque traits
-	**jrpTravel** - Alternative to multiple traveling systems, with teleporters between Telvanni settlements and all Mage Guild locations accessible from each guild transporter
-	**jrpWerewolf** - Command to "fake" being a werewolf
-	**lastLogin** - See when a player was last online
-	**noticeBoard** - Public book players can read/write entries into
-	**portableBedroll** - Bedrolls can be picked up like an item and still slept on
-	**questCounter** - Percentage tracker of how many quests a player has done in each region and faction
-	**racialPerks** - Racial bonuses made for JRP
-	**rpSpeedBoost** - Sets a players speed higher for roleplay purposes
-	**setSex** - Command to change a players sex without logging off
-	**simpleRevive** - Activating a dead player revives them with a fraction of their health back.
-	**xCard** - Player safety tool for roleplay

### Fixes
-	**jrpFixes** - Simple script to add common quest/bug fixes
-	**speechAntiCrash** - Prevents server from crashing if custom race uses speech command

### Utilities
-	**CellInserter** - A framework to easily add or remove objects from the world after cell resets
-	**cellManualBackup** - Adds command to backup the current cell json into a custom directory
-	**cloneCell** -  Adds command to duplicate a cells contents into another cell
-	**itemCatalog** - A framework that creates a large list of refIds' and their item names
-	**multiDoor** - A script to give doors admin-defined multiple destinations
-	**urmCellResetCheck** - Announces cell reset exclusion when the player places/picks up anything in a cell, for urm's CellReset

### Mod Support
-	**Wizard Hats** - Requires mod by Daduke. Replaces items with custom records
