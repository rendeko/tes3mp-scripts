-- balmoraFixMe for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- under BSD

local balmoraFixMe = {}

local function ChatListener(pid,cmd)
	if cmd[1] == "fixtp" then
		tes3mp.LogMessage(2, "[balmoraFixMe] " .. Players[pid].name .. " used /fixtp and moved to Balmora")
		tes3mp.SetCell(pid, "-3, 2")
		tes3mp.SetPos(pid, -23994,-15365.4,505)
		tes3mp.SendCell(pid)
		tes3mp.SendPos(pid)
		tes3mp.SendMessage(pid, "You've been moved to Balmora using fixtp.\n", false)
	end
end

customCommandHooks.registerCommand("fixtp", ChatListener)
