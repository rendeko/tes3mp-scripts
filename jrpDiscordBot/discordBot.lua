-- jrpPlayerList bot by Malic for JRP Roleplay
-- don't run with TES3MP!!! Run with Luvit. Ideally, as a systemd service.
-- under GPLv3

local discordia = require('discordia')
local client = discordia.Client()

-- DISCORD_TOKEN = "YOURTOKENHERE"
-- CHANNEL_ID = "YOURIDHERE"
INDEXPATH = "/home/chelsea/TES3MP-deploy/build/index.md"

local function CheckPosts()
	local clock = discordia.Clock()
	clock:on("min", function(d)
		if d.min % 5 == 0 then
			print("clock is interval of 5. running...")
			local channel = client:getChannel(CHANNEL_ID)
			
			local messages = channel:getMessages(1)
			if not messages then -- request failed
				print("request failed. waiting until next clock point.")
			elseif #messages == 0 then
				print("channel is empty")
				file = io.open(INDEXPATH, "r") -- read file
				channel:send(file:read'*a') -- send all contents
				file:close() -- close file
			else
				print("channel has post in it. attempting to edit")
				local message = channel:getLastMessage()
				file = io.open(INDEXPATH, "r") -- read file
				message:setContent(file:read'*a')
				message:setEmbed {
				  description = "edited",
				  color = discordia.Color("#FFFFFF").value
				}
				file:close() -- close file
				print("post edited")
			end
		end
	end)
	clock:start()
end

client:on('ready', function()
	print('Logged in as '.. client.user.username)
	CheckPosts()
end)

client:run('Bot ' .. DISCORD_TOKEN)
