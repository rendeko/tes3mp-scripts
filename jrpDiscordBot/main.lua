-- jrpPlayerList for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- writes a simple txt file with player count every x minutes, used with Discord bot
-- under GPLv3

-- to-do: nginx and proper html writing instead of Discord markdown

local jrpPlayerList = {}
jrpPlayerList.config = {}
jrpPlayerList.config.updateTime = 300000 -- update time in milliseconds (300000 = 5min)
jrpPlayerList.config.serverName = "JRP Roleplay" -- server name
jrpPlayerList.config.fileDir = tes3mp.GetModDir() -- don't change this

local function getReadableTime()
	return os.date("%m/%d/%Y %I:%M %p")
end

local function startTimer()
tes3mp.StartTimer(
	tes3mp.CreateTimerEx(
		"jrpPlayerListUpdateTimer",
		jrpPlayerList.config.updateTime,
		"i",
		0)
	)
end

local function listPlayers()
	local lastPid = tes3mp.GetLastPlayerId()
	local list = ""
	local divider = ""

	for playerIndex = 0, lastPid do
		if playerIndex == lastPid then
			divider = ""
		else
			divider = "\n"
		end
		if Players[playerIndex] ~= nil and Players[playerIndex]:IsLoggedIn() then
			local targetCell = ""
			targetCell = tes3mp.GetCell(playerIndex)
			list = list .. tostring(Players[playerIndex].name) .. " | " .. targetCell .. divider
		end
	end
	
	if list == "" then
		list = "No players online."
	end
	
	return list
end

function jrpPlayerListUpdateTimer() -- timers have to be global(?)
	local filePath = jrpPlayerList.config.fileDir .. "index.md"
	local file = io.open("index.md","w")
	file:write(
	"```" .. jrpPlayerList.config.serverName .. "\n" ..
	getReadableTime() .. "\n \n" ..
	listPlayers() .. "```"
	)
	file:close()
	
	startTimer() -- start timer again
end

local function OnServerPostInitHandler(pid) -- placeholder until someone generates a PID
	local filePath = jrpPlayerList.config.fileDir .. "index.md"
	local file = io.open("index.md","w")
	file:write(
	"```" .. jrpPlayerList.config.serverName .. "\n" ..
	getReadableTime() .. "\n \n" ..
	"No players online." .. "```"
	)
	file:close()
	tes3mp.LogMessage(2, "[jrpPlayerList] Initialized")
end

jrpPlayerList.toggled = false -- only check for PIDs on a successful connection, otherwise it'll crash
local function OnPlayerAuthentifiedHandler(pid)
	if jrpPlayerList.toggled == false then
		jrpPlayerListUpdateTimer()
		jrpPlayerList.toggled = true
	end
end

customEventHooks.registerHandler("OnServerPostInit", OnServerPostInitHandler)
customEventHooks.registerHandler("OnPlayerAuthentified", OnPlayerAuthentifiedHandler)
