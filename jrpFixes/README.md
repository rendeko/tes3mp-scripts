## jrpFixes (for 0.7-alpha)
-	An attempt at making everything in Morrowind+DLC work in TES3MP
-	Under GPLv3

### Installation
-	Add jrpFixes_config.json to data/custom
-	Add jrpFixes.lua to scripts/custom/
-	Add to customScripts.lua
		`require("custom.jrpFixes")`

### Features
-	Working Seyda Neen census
-	Quest-skip

### To-do
-	Make tax guy appear in Census
-	Make stronghold/raven-rock quests work without just skipping them
-	Persistent world saving or said strongholds/raven-rock
