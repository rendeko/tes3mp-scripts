-- this one's not even a script, actually! just a documented change
-- thanks to David C in the TES3MP discord.

-- the following in config.lua will disable DB assassins
config.disallowedCreateRefIds = { "db_assassin1", "db_assassin1b", "db_assassin2", "db_assassin3", "db_assassin4" }
