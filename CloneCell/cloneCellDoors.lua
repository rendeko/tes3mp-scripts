-- cloneCellDoors for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- /clonedoors copies all doors from one cell into the current cell
-- requires espParser, struct, DataManager, DoorFramework
-- under GPLv3

-- don't commit until you make DoorFramework work with local var!

local doInfo = function(text)
    tes3mp.LogMessage(enumerations.log.INFO, text) 
end

local function CopyDoors(eventStatus,pid,currentCell,requestedCell)
	doInfo("[cloneCellDoors] Start")
	for _, file in pairs(espParser.files) do 
		for cellName, cell in pairs(file.cells) do
			if cellName == requestedCell then
				Players[pid]:Message(color.Gray .. "Found requested cell " .. cellName .. " in ESP\n" .. color.Default)
				for _, obj in pairs(cell.objects) do
					if obj.doorDest ~= nil and obj.pos ~= nil then
						tes3mp.LogMessage(2, "Found " .. obj.refId .. ". Copying...")
						
						origLocation = {}
						origLocation.posX = obj.pos["XPos"]
						origLocation.posY = obj.pos["YPos"]
						origLocation.posZ = obj.pos["ZPos"]
						origLocation.rotX = obj.pos["XRot"]
						origLocation.rotY = obj.pos["YRot"]
						origLocation.rotZ = obj.pos["ZRot"]
						tes3mp.LogMessage(2, "origLocation set")
						
						origDest = {}
						origDest.posX = obj.doorDest["XPos"]
						origDest.posY = obj.doorDest["YPos"]
						origDest.posZ = obj.doorDest["ZPos"]
						origDest.rotX = obj.doorDest["XRot"]
						origDest.rotY = obj.doorDest["YRot"]
						origDest.rotZ = obj.doorDest["ZRot"]
						tes3mp.LogMessage(2, "origDest set")
						
						if obj.destCell == nil then -- destCell is nil if destination is exterior cell
							origDestCell = "0, 0"
							origDestName = "Leave" -- automate finding out exterior cell name later
						else
							origDestCell = obj.destCell
							origDestName = "Door to " .. obj.destCell
						end
						tes3mp.LogMessage(2, "origDestCell set")
						tes3mp.LogMessage(2, "origDestName set")
						
						local continueVar = false
					
						
						for _,record in pairs(espParser.getAllRecords("DOOR")) do -- check all door records
							for i, subrecord in pairs(record.subRecords) do
								if subrecord.name == "NAME" then
									if obj.refId == struct.unpack("s", subrecord.data) then -- find the door record we want to get the model path from
										continueVar = true -- only go further if door refID matches esp door refID
										tes3mp.LogMessage(2, "Found door reference that matches obj.refId")
									end
								end
								if subrecord.name == "MODL" and continueVar == true then
									origModel = struct.unpack("s", subrecord.data)
									tes3mp.LogMessage(2, "origModel set to " .. origModel)
								end
							end
							continueVar = false
						end
						
						if origLocation ~= nil and origDest ~= nil and origModel ~= nil then
							local recordStore = RecordStores["miscellaneous"]
							local recordId = recordStore:GenerateRecordId()
							DoorFramework.createRecord(recordId,origDestName,origModel,origDestCell,origDest,DoorFramework.config.sound)
							recordStore:Save()
							recordStore:LoadRecords() -- send new record to client
							DataManager.saveData(DoorFramework.scriptName, DoorFramework.data) -- save to doorframework
							
							tes3mp.LogMessage(2, "Spawning " .. recordId .. " in " .. currentCell)
							DoorFramework.spawnDoor(recordId,currentCell,{
								posX = origLocation.posX,
								posY = origLocation.posY,
								posZ = origLocation.posZ,
								rotX = origLocation.rotX,
								rotY = origLocation.rotY,
								rotZ = origLocation.rotZ
							})
							Players[pid]:Message(color.Gray .. "Done!\n" .. color.Default)
						else
							Players[pid]:Message(color.Red .. "Error.\n" .. color.Default)
						end
					end
				end
			end
		end
	end
	LoadedCells[currentCell]:Save()
end

local function ChatListener(pid, cmd)
	local cellDescription = tes3mp.GetCell(pid)
	if Players[pid]:IsAdmin() then
		if cmd[1] == "clonedoors" and cmd[2] ~= nil then
			CopyDoors(eventStatus,pid,cellDescription,table.concat(cmd, " ", 2))
		end
	end
end

customCommandHooks.registerCommand("clonedoors", ChatListener)
