-- cloneCell for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- /clonecell copies all records from requested cell in ESP and places them in current cell, server side
-- /clonecellall does the above but also copies all server placed items
-- requires espParser, struct, DataManager
-- under GPLv3

local doInfo = function(text)
    tes3mp.LogMessage(enumerations.log.INFO, text) 
end

local function CopyCell(eventStatus,pid,currentCell,requestedCell)
	doInfo("[cloneCell] Start")
	for _, file in pairs(espParser.files) do    
		for cellName, cell in pairs(file.cells) do
			if cellName == requestedCell then
				Players[pid]:Message(color.Gray .. "Found requested cell " .. cellName .. " in ESP\n" .. color.Default)
				for _, obj in pairs(cell.objects) do
					if obj.pos ~= nil and obj.doorDest == nil then -- don't spawn doors. use cloneCellDoors for that
						location = {}
						location.posX = obj.pos["XPos"]
						location.posY = obj.pos["YPos"]
						location.posZ = obj.pos["ZPos"]
						location.rotX = obj.pos["XRot"]
						location.rotY = obj.pos["YRot"]
						location.rotZ = obj.pos["ZRot"]
						logicHandler.CreateObjectAtLocation(currentCell,location,obj.refId,"place")
					end
				end
			end
		end
	end
	LoadedCells[currentCell]:Save()
end

local function CopyServerCell(eventStatus,pid,currentCell,requestedCell)
	if LoadedCells[requestedCell] == nil then
		logicHandler.LoadCell(requestedCell)
	end
	
	local data = LoadedCells[requestedCell].data
	
	for uniqueIndex,obj in pairs(data.objectData) do
		--tes3mp.LogMessage(2, "placing " .. obj.refId .. " aka " .. uniqueIndex)
		if obj.location ~= nil then
			location = {}
			location.posX = obj.location["posX"]
			location.posY = obj.location["posY"]
			location.posZ = obj.location["posZ"]
			location.rotX = obj.location["rotX"]
			location.rotY = obj.location["rotY"]
			location.rotZ = obj.location["rotZ"]
		end
		logicHandler.CreateObjectAtLocation(currentCell,location,obj.refId,"place")
	end
	LoadedCells[currentCell]:Save()
end

local function ChatListener(pid, cmd)
	local cellDescription = tes3mp.GetCell(pid)
	if Players[pid]:IsAdmin() then
		if cmd[1] == "clonecell" and cmd[2] ~= nil then
			CopyCell(eventStatus,pid,cellDescription,table.concat(cmd, " ", 2))
			Players[pid]:Message(color.Gray .. "Done!\n" .. color.Default)
		elseif cmd[1] == "clonecellall" and cmd[2] ~= nil then
			CopyCell(eventStatus,pid,cellDescription,table.concat(cmd, " ", 2))
			CopyServerCell(eventStatus,pid,cellDescription,table.concat(cmd, " ", 2))
			Players[pid]:Message(color.Gray .. "Done!\n" .. color.Default)
		end
	end
end

customCommandHooks.registerCommand("clonecell", ChatListener)
customCommandHooks.registerCommand("clonecellall", ChatListener)