## cloneCell (for 0.7-alpha)
-	Copies the contents of another cell into your current cell
-	Made to create "instances" of the same cell
-	Under GPLv3

This isn't very useful until custom cell records are implemented, unless you have an ESP that adds blank cell records.

Requires [espParser](https://github.com/JakobCh/tes3mp_scripts/blob/master/espParser/)

### Installation
-	Install [DataManager](https://github.com/tes3mp-scripts/DataManager) by uramer
-	Install [espParser](https://github.com/JakobCh/tes3mp_scripts/blob/master/espParser/) by Jakob
-	Add cloneCell.lua to scripts/custom/
-	Add to customScripts.lua somewhere under DataManager, espParser, and struct's line:
		`require("custom.cloneCell")`

### Usage
-	/clonecell copies all records from requested cell from an ESP and places them in current cell, server side
-	/clonecellall does the above but also copies all server placed items

### To-do
-	Add check that requestedCell actually exists
-	Add custom cell record support (when it exists)
-	Split into cloneto and clonefrom commands
