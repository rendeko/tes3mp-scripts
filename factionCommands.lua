-- factionCommands for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- two commands to unexpell yourself/leave factions
-- under BSD

local factions = {}
factions.entries = {"blades","east empire company","fighters guild","hlaalu","redoran","telvanni","imperial legion","mages guild","thieves guild"}

table.insert(guiHelper.names, "factionCommands_gui")
guiHelper.ID = tableHelper.enum(guiHelper.names)

local function ClearTable(pid)
	factions[pid] = ""
end

local function ShowFactionMenu(eventStatus,pid,guiID,message)
	local message = ""
	if factions[pid] == "unexpell" then
		message = "Select faction to be unexpelled from"
	elseif factions[pid] == "leave" then
		message = "Select faction to leave"
	end
	table.sort(factions.entries)
	local list = "*CLOSE*\n" .. table.concat(factions.entries,"\n")
	tes3mp.ListBox(pid,guiHelper.ID.factionCommands_gui,message,list)
end

local function CheckGUI(newStatus,pid,idGui,data)
	if idGui == guiHelper.ID.factionCommands_gui then
		if tonumber(data) ~= 0 and tonumber(data) < 5000000 and factions[pid] == "leave" then
			if Players[pid].data.factionRanks[factions.entries[tonumber(data)]] ~= nil then
				tes3mp.LogMessage(2, "[factionCommands] " .. Players[pid].name .. " removed themselves from " .. factions.entries[tonumber(data)])
				Players[pid].data.factionRanks[factions.entries[tonumber(data)]] = nil
				if Players[pid].data.factionExpulsion[factions.entries[tonumber(data)]] ~= nil then
					Players[pid].data.factionExpulsion[factions.entries[tonumber(data)]] = nil
				end
				Players[pid]:Save()
				tes3mp.MessageBox(pid, -1, "You've been removed from that faction. You may need to relog.")
			else
				tes3mp.MessageBox(pid, -1, "You aren't in that faction.")
				tes3mp.LogMessage(2, "[factionCommands] " .. Players[pid].name .. " attempted to remove themselves from " .. factions.entries[tonumber(data)] .. ", but they aren't in it.")
			end
		elseif tonumber(data) ~= 0 and tonumber(data) < 5000000 and factions[pid] == "unexpell" then
			if Players[pid].data.factionExpulsion[factions.entries[tonumber(data)]] ~= nil then
				if Players[pid].data.factionExpulsion[factions.entries[tonumber(data)]] == true then
					tes3mp.LogMessage(2, "[factionCommands] " .. Players[pid].name .. " unexpelled themselves from " .. factions.entries[tonumber(data)])
					Players[pid].data.factionExpulsion[factions.entries[tonumber(data)]] = false
					logicHandler.RunConsoleCommandOnPlayer(pid, "pcclearexpelled " .. factions.entries[tonumber(data)]) -- so it acts immediately
					Players[pid]:Save()
					tes3mp.MessageBox(pid, -1, "You've been unexpelled from that faction. You may need to relog.")
				else
					tes3mp.MessageBox(pid, -1, "You aren't expelled by that faction.")
				tes3mp.LogMessage(2, "[factionCommands] " .. Players[pid].name .. " attempted to unexpell themselves from " .. factions.entries[tonumber(data)] .. ", but they aren't expelled.")
				end
			else
				tes3mp.MessageBox(pid, -1, "You aren't expelled by that faction.")
				tes3mp.LogMessage(2, "[factionCommands] " .. Players[pid].name .. " attempted to unexpell themselves from " .. factions.entries[tonumber(data)] .. ", but they aren't expelled.")
			end
		end
		ClearTable(pid)
	end
end

local function ChatListener(pid,cmd)
	if cmd[1] == "leavefaction" then
		factions[pid] = "leave"
		ShowFactionMenu(eventStatus,pid,guiHelper.ID.factionCommands_gui,message)
	elseif cmd[1] == "unexpell" then
		factions[pid] = "unexpell"
		ShowFactionMenu(eventStatus,pid,guiHelper.ID.factionCommands_gui,message)
	end
end

customEventHooks.registerValidator("OnGUIAction", CheckGUI)
customEventHooks.registerHandler("OnPlayerAuthentified", ClearTable)
customEventHooks.registerHandler("OnPlayerDisconnect", ClearTable)
customCommandHooks.registerCommand("leavefaction", ChatListener)
customCommandHooks.registerCommand("unexpell", ChatListener)
