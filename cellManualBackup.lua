-- cellManualBackup for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- simple command to copy a cell json to another folder, for really impressive player content
-- under GPLv3

local cellDir = tes3mp.GetModDir() .. "/cell/"
local backupDir = tes3mp.GetModDir() .. "/custom/cellManualBackup/" -- make sure to create this folder before running

local function ChatListener(pid,cmd)
	if cmd[1] == "backupcell" and cmd[2] == nil and Players[pid]:IsAdmin() then
		local cellDescription = tes3mp.GetCell(pid)
		local cell = Cell(cellDescription)
		local cellFilePath = cellDir .. cell.entryFile
		local outputFile = backupDir .. cell.entryFile
		
		if tes3mp.DoesFileExist(cellFilePath) then
			local inFile = io.open(cellFilePath,"r")
			local inContent = inFile:read("*a")
			inFile:close()
			local outFile = io.open(outputFile,"w")
			outFile:write(inContent)
			outFile:close()
			local newFilename = backupDir .. os.date("%Y%m%d%H%M%S") .. " " .. cell.entryFile
			os.rename(outputFile,newFilename)
			Players[pid]:Message(color.SlateGray .. cellDescription .. " saved to " .. newFilename .. color.Default .. "\n")
			tes3mp.LogMessage(2, "[cellManualBackup] " .. Players[pid].name .. "'used backupcell on " .. cellDescription)
		end
	end
end

customCommandHooks.registerCommand("backupcell", ChatListener)
