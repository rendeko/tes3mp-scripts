## CellInserter (for 0.7-alpha)
-	A framework/API to insert/remove objects on cell reset, allowing for seemingly "persistent" objects
-	Example config fixes some TR_Preview bugs, adds a small bench outside of Ald-Ruhn Manor District.
-	Supports Uramer-CellReset and Atkana-CellReset style cell resets
-	Data file loads from config file, and is added to by other scripts
-	Under GPLv3

### Installation
-	Install [DataManager](https://github.com/tes3mp-scripts/DataManager) by uramer (yes, my framework requires another framework...)
-	Add main.lua and defaultConfig.lua to scripts/custom/CellInserter/
-	Add to customScripts.lua somewhere under DataManager's line:
		`require("custom.CellInserter.main")`

### Addons
#### addon-esp
This addon uses Jakob's [espParser](https://github.com/JakobCh/tes3mp_scripts/blob/master/espParser/) to place all objects from an ESP into their corresponding cells on load. Effectively, this negates the need for an ESP in exchange for a large performance hit (making it not recommended to use this addon). Object removals have not been implemented yet.

-	Add addon-esp.lua to scripts/custom/CellInserter/
-	Add to customScripts.lua under CellInserter, espParser, and struct's lines:
		`require("custom.CellInserter.addon-esp")`

### To-do
-	Add scaling
