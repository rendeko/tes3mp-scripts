local defaultConfig = {
	insert = {
		["-2, 7"] = {
			["insertCount"] = 1,
			["1"] = { -- bench outside ald-ruhn manor district
				["refId"] = "furn_de_r_bench_01",
				["pos"] = {-12598.5,57827,2618.75,0,0,0.4},
				["scale"] = 1
			}
		}
	},
	remove = {
		["-2, 7"] = {"82799-0","82797-0","49157-0","82798-0"}, -- some plants outside of ald-ruhn manor district
		["Salvarys Ancestral Tomb"] = {"149298-0","149299-0","149301-0","149302-0","149303-0","149305-0","149306-0","149307-0","149309-0"}, -- TR_Mainland, light script crashes client
		["3, -27"] = {"87990-0"}, -- TR_Preview, Oran Plantation, Helseri Farm door. Spams OnObjectLock
		["Bodrem, Hall"] = {"347905-0"} -- TR_Preview, Bodrem Hall, Wesencolm. Spams OnPlayerJournal
	}
}

return defaultConfig
