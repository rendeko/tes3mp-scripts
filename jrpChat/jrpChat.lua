-- jrpChat for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- script that revamps how chat looks and feels, inspired by roleplay-chat-essentials
-- inspired by roleplayChatEssentials by David-AW
-- special thanks to Rickoff for distance based chat from ChatConfig!
-- under GPLv3

-- /whisper or /w travels 1 meter. Players within 5 meters see the whispering but can't hear the contents.
-- normal speaking travels 5 meters
-- /shout or /s and /me travels 10 meters
-- /// (looc) travels through the entire cell
-- // (ooc) travels through the entire world


local jrpChat = {}
jrpChat.config = {}
jrpChat.config.disable_greentext = true
jrpChat.config.languages = {
		["alt"] = "Aldmeri",
		["arg"] = "Jel",
		["bos"] = "Bosmeri",
		["dae"] = "Daedric",
		["dra"] = "Draconic",
		["dun"] = "Dunmeri",
		["kha"] = "Ta'agra",
		["nor"] = "Nordic",
		["orc"] = "Orcish",
		["slo"] = "Sload",
		["yok"] = "Yoku"
}
jrpChat.config.whisperDistance = 175
jrpChat.config.speakDistance = 500 -- 5 meters
jrpChat.config.shoutDistance = 1000 -- 10 meters


local function resetNickname(pid)
	Players[pid].data.customVariables.jrpChatName = Players[pid].name
	Players[pid]:Save()
	tes3mp.LogMessage(2, "[jrpChat] " .. Players[pid].name .. "'s nickname has been reset.")
end

local function setNickname(pid,cmd)
	if cmd[2] ~= nil then
		Players[pid].data.customVariables.jrpChatName = table.concat(cmd, " ", 2)
		Players[pid]:Save()
		tes3mp.LogMessage(2, Players[pid].accountName .. " set their nickname to " .. Players[pid].data.customVariables.jrpChatName)
		Players[pid]:Message(color.SlateGray .. "Your nickname has been set to " .. Players[pid].data.customVariables.jrpChatName .. color.Default .. "\n")
		return
	else
		resetNickname(pid)
	end
end

-- nickname seems to crash after a while? lets make sure the variable exists
local function nicknameGuarantee(pid)
	if Players[pid].data.customVariables.jrpChatName == nil then
		tes3mp.LogMessage(2, "[jrpChat] " .. Players[pid].name .. " found with jrpChatName undefined. Setting to their regular name.")
		Players[pid].data.customVariables.jrpChatName = Players[pid].name
		Players[pid]:Save()
	end
end

local function SendLocalMessage(pidSender,message,state) -- thanks rickoff!
	nicknameGuarantee(pidSender)
	local cellDescription = Players[pidSender].data.location.cell	
	
	local radius = 1
	if state == "whisper" then
		radius = jrpChat.config.whisperDistance
		speakType = " (whisper)"
	elseif state == "speak" then
		radius = jrpChat.config.speakDistance
		speakType = ""
	elseif state == "shout" then
		radius = jrpChat.config.shoutDistance
		speakType = " (shout)"
	end
	
	if Players[pidSender].name == Players[pidSender].data.customVariables.jrpChatName then
		playerName = color.Beige .. Players[pidSender].name
	else
		playerName = color.SlateGray .. Players[pidSender].data.customVariables.jrpChatName
    end
	
	if message:sub(1, 1) == '!' then
		message = tableHelper.concatenateFromIndex(langMsg, 2)
	end

	
	local senderPosX = tes3mp.GetPosX(pidSender)
	local senderPosY = tes3mp.GetPosY(pidSender)
	local senderPosZ = tes3mp.GetPosZ(pidSender)
	for index,pid in pairs(LoadedCells[cellDescription].visitors) do
		if Players[pid].data.location.cell == cellDescription then
			local recievePosX = tes3mp.GetPosX(pid)
			local recievePosY = tes3mp.GetPosY(pid)
			local recievePosZ = tes3mp.GetPosZ(pid)
			local distance = math.sqrt((senderPosX - recievePosX) * (senderPosX - recievePosX) + (senderPosY - recievePosY) * (senderPosY - recievePosY))
			if distance < radius then
				local finalMessage = playerName .. langText .. speakType .. ": " .. color.Default .. message .. "\n" -- only regular speaking handles language so far
				tes3mp.SendMessage(pid,finalMessage,false)
			elseif state == "whisper" and distance < jrpChat.config.speakDistance then -- see others whisper but not understand it at speaking range
				local finalMessage = color.Default .. "You see " .. playerName .. " whisper, but you can't understand it from here.\n"
				tes3mp.SendMessage(pid,finalMessage,false)
			elseif state == "speak" and distance < jrpChat.config.shoutDistance then -- hear someone speak but not understand it at yelling range
				local finalMessage = color.Default .. "You hear " .. playerName .. " speak, but you can't understand it from here.\n"
				tes3mp.SendMessage(pid,finalMessage,false)
			elseif state == "shout" and distance > shoutDistance then -- hear someone shout but not understand it outside of yelling range
				local finalMessage = color.Default .. "You hear someone shout, but you can't understand it from here.\n"
				tes3mp.SendMessage(pid,finalMessage,false)
			end
		end
	end
end

local function OnPlayerSendMessage(eventStatus,pid,message)
	nicknameGuarantee(pid)

	if message:sub(1, 1) == '/' then -- don't act on anything that starts with a slash
		return
	elseif message:sub(1, 1) == '!' then
		langMsg = (message:sub(2, #message)):split(" ")
		langAvailable = {}
		foundLang = false
		for langCmd,langName in pairs(jrpChat.config.languages) do
			table.insert(langAvailable, langCmd)
			if langMsg[1] == langCmd then
				selectedLang = langName
				foundLang = true
			end
		end
		if foundLang == false then
			table.sort(langAvailable)
			Players[pid]:Message(color.SlateGray .. "Languages: " .. table.concat(langAvailable,", ") .. color.Default .. "\n")
			return customEventHooks.makeEventStatus(false, nil)
		end
		langText = color.Gray .. " (in " .. selectedLang .. ")"
	else
		langText = ""
	end
    
    if Players[pid].name == Players[pid].data.customVariables.jrpChatName then -- color handling
		playerName = color.Beige .. Players[pid].name
	else
		playerName = color.SlateGray .. Players[pid].data.customVariables.jrpChatName
    end
	
	SendLocalMessage(pid,message,"speak")
	return customEventHooks.makeEventStatus(false, nil)
end

local function ChatListener(pid, cmd) -- replace later
	--nickname commands
	if cmd[1] == "nickname" and cmd[2] ~= nil then
		setNickname(pid,cmd)
	elseif cmd[1] == "nickname" and cmd[2] == nil then
		resetNickname(pid)
		Players[pid]:Message(color.SlateGray .. "Your nickname has been reset.\n" .. color.Default)
		return
	end
end

local function ChatOOC(pid,cmd)
	if cmd[1] == "/" then
		local message = color.Orange .. "[OOC] " .. color.Default .. Players[pid].name .. " (" .. pid .. ")" .. ": " .. color.Gray
		message = message .. tableHelper.concatenateFromIndex(cmd, 2) .. "\n"
		tes3mp.SendMessage(pid, message, true)
	end
end

local function ChatLOOC(pid,cmd)
	if cmd[1] == "//" then
		local cellDescription = Players[pid].data.location.cell
		if logicHandler.IsCellLoaded(cellDescription) == true then
			for index, visitorPid in pairs(LoadedCells[cellDescription].visitors) do
				local message = color.RosyBrown .. "[LOOC] " .. color.Default .. Players[pid].data.customVariables.jrpChatName .. " (" .. pid .. ")" .. ": "  .. color.Gray
				message = message .. tableHelper.concatenateFromIndex(cmd, 2) .. "\n"
				tes3mp.SendMessage(visitorPid, message, false)
			end
		end
	end
end

local function ChatAction(pid,cmd)
	if cmd[1] == "me" then
		local cellDescription = Players[pid].data.location.cell
		if logicHandler.IsCellLoaded(cellDescription) == true then
			for index, visitorPid in pairs(LoadedCells[cellDescription].visitors) do
				local message = color.Red .. "[ACTION] " .. color.Default .. Players[pid].data.customVariables.jrpChatName .. " "
				message = message .. tableHelper.concatenateFromIndex(cmd, 2) .. "\n"
				tes3mp.SendMessage(visitorPid, message, false)
			end
		end
	end
end

local function ChatDisable(pid,cmd)
	if cmd[1] == "gt" and jrpChat.config.disable_greentext == true then
		return
	elseif cmd[1] == "l" then
		return
	end
end

local function ChatRadial(pid,cmd)
	local cellDescription = Players[pid].data.location.cell
	message = tableHelper.concatenateFromIndex(cmd, 2)
	langText = "" -- disable language for shouts/whispers. maybe implement later
	if (cmd[1] == "shout" or cmd[1] == "s") and cmd[2] ~= nil then
		SendLocalMessage(pid,message,"shout")
	elseif (cmd[1] == "whisper" or cmd[1] == "w") and cmd[2] ~= nil then
		SendLocalMessage(pid,message,"whisper")
	end
end

local function OnPlayerFinishLogin(eventStatus,pid)
	resetNickname(pid)
end

customEventHooks.registerHandler("OnPlayerFinishLogin", OnPlayerFinishLogin)
customEventHooks.registerValidator("OnPlayerSendMessage", OnPlayerSendMessage)
customCommandHooks.registerCommand("nickname", ChatListener)
customCommandHooks.registerCommand("w", ChatRadial)
customCommandHooks.registerCommand("whisper", ChatRadial)
customCommandHooks.registerCommand("s", ChatRadial)
customCommandHooks.registerCommand("shout", ChatRadial)
customCommandHooks.registerCommand("/", ChatOOC)
customCommandHooks.registerCommand("//", ChatLOOC)
customCommandHooks.registerCommand("me", ChatAction)
customCommandHooks.registerCommand("gt", ChatDisable)
customCommandHooks.registerCommand("l", ChatDisable)
