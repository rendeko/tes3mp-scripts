## jrpChat (for 0.7)
-	A roleplay based chat overhaul
-	All chat is local by default except for OOC
-	Supports nicknames, languages, distance
-	Under GPLv3

![preview](preview.jpg)

### Installation
-	Add jrpChat.lua to scripts/custom/
-	Add to customScripts.lua:
		`require("custom.jrpChat")`

### Commands
-   **no command -** Speak to whoever is within 5 meters
-	**/shout text -** Speak to whoever is within 10 meters (can also use **/s**)
-	**/whisper text -** Speak to whoever is within 1 meter (can also use **/w**)
-	**// text -** Use global OOC chat
-	**/// text -** Use local OOC chat (to everybody in your current cell)
-	**/me does a dance -** Perform an action. Example: *[ACTION] JohnSmith does a dance*
-	**/nickname text -** Sets your nickname
-	**/nickname -** Resets your nickname
-	**!lang -** See the languages you can speak
-	**!lang hello -** Send a local message in another language

### To-do
-   Tidy this up, remove redundant functions
