-- rpSpeedBoost for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- script that sets a players speed to a standard. for roleplay purposes
-- under BSD

local rpSpeedBoost = {}

local function OnPlayerAuthentifiedHandler(eventStatus,pid)
	if Players[pid].data.customVariables.rpSpeedBoost == nil then
		Players[pid].data.customVariables.rpSpeedBoost = {}
	end
end

local function SetBoost(pid)
	if Players[pid].data.customVariables.rpSpeedBoost.oldSpeed == nil and Players[pid].data.customVariables.rpSpeedBoost.oldAthletics == nil then
		Players[pid].data.customVariables.rpSpeedBoost.oldSpeed = Players[pid].data.attributes.Speed.base
		Players[pid].data.customVariables.rpSpeedBoost.oldAthletics = Players[pid].data.skills.Athletics.base
		Players[pid].data.attributes.Speed.base = 50
		Players[pid].data.skills.Athletics.base = 50
		Players[pid]:Save()
		Players[pid]:LoadAttributes()
		Players[pid]:LoadSkills()
		Players[pid]:Message(color.SlateGray .. "Your speed/athletics has been set to 50.\n" .. color.Default)
		tes3mp.LogMessage(2, "[rpSpeedBoost] " .. Players[pid].name .. " boosted their speed.")
	end
end

local function RevertBoost(pid)
	if Players[pid].data.customVariables.rpSpeedBoost.oldSpeed ~= nil and Players[pid].data.customVariables.rpSpeedBoost.oldAthletics ~= nil then
		Players[pid].data.attributes.Speed.base = Players[pid].data.customVariables.rpSpeedBoost.oldSpeed
		Players[pid].data.skills.Athletics.base = Players[pid].data.customVariables.rpSpeedBoost.oldAthletics
		Players[pid].data.customVariables.rpSpeedBoost.oldSpeed = nil
		Players[pid].data.customVariables.rpSpeedBoost.oldAthletics = nil
		Players[pid]:Save()
		Players[pid]:LoadAttributes()
		Players[pid]:LoadSkills()
		Players[pid]:Message(color.SlateGray .. "Your speed/athletics has been reverted.\n" .. color.Default)
		tes3mp.LogMessage(2, "[rpSpeedBoost] " .. Players[pid].name .. " reverted their speed.")
	end
end

local function ChatListener(pid, cmd)
	if cmd[1] == "speedboost" and cmd[2] == "on" then
		SetBoost(pid)
	elseif cmd[1] == "speedboost" and cmd[2] == "off" then
		RevertBoost(pid)
	end
end

customCommandHooks.registerCommand("speedboost", ChatListener)
customEventHooks.registerHandler("OnPlayerAuthentified", OnPlayerAuthentifiedHandler)
