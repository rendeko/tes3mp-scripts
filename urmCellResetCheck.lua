-- urmCellResetCheck for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- Shows cell reset status as a buff in the buff bar, for urm's CellReset
-- under BSD

local function OnServerInitHandler()
	local recordStore = RecordStores["spell"]
	recordStore.data.permanentRecords["urmCellResetCheck_resets"] = {
		name = "This cell does not save",
		subtype = 4,
		cost = 0,
		flags = 0,
		effects = {{
			["id"] = 126,
			["attribute"] = -1,
			["skill"] = -1,
			["rangeType"] = 0,
			["area"] = 0,
			["magnitudeMin"] = 0,
			["magnitudeMax"] = 0
		}}
	}
	recordStore:Save()
	recordStore.data.permanentRecords["urmCellResetCheck_noreset"] = {
		name = "This cell saves on reset",
		subtype = 4,
		cost = 0,
		flags = 0,
		effects = {{
			["id"] = 126,
			["attribute"] = -1,
			["skill"] = -1,
			["rangeType"] = 0,
			["area"] = 0,
			["magnitudeMin"] = 0,
			["magnitudeMax"] = 0
		}}
	}
	recordStore:Save()
end

local function OnPlayerCellChangeHandler(eventStatus,pid)
	local cellDescription = tes3mp.GetCell(pid)
	playerMatch = {}
	playerMatch[pid] = false
	for excludedCellDescription,_ in pairs(CellReset.data.excludedCells) do
		if cellDescription == excludedCellDescription then
			playerMatch[pid] = true
		end
	end
	if playerMatch[pid] == true then
		-- changes constantly and has no effects, so no need to sync with spellbook
		logicHandler.RunConsoleCommandOnPlayer(pid, "player->addspell urmCellResetCheck_noreset")
		logicHandler.RunConsoleCommandOnPlayer(pid, "player->removespell urmCellResetCheck_resets")
	else
		logicHandler.RunConsoleCommandOnPlayer(pid, "player->removespell urmCellResetCheck_noreset")
		logicHandler.RunConsoleCommandOnPlayer(pid, "player->addspell urmCellResetCheck_resets")
	end
end

customEventHooks.registerHandler("OnServerInit", OnServerInitHandler)
customEventHooks.registerHandler("OnPlayerCellChange", OnPlayerCellChangeHandler)