-- jrpShop addon for Westly's Fine Clothiers of Tamriel

require("custom.jrpShop.main")

local helmets = {
	["Turban of Elsweyr (Brown)"] = {["id"] = "WAR_KHAJIIT_TURBAN01", ["cost"] = 20},
	["Turban of Elsweyr (White)"] = {["id"] = "WAR_KHAJIIT_TURBAN02", ["cost"] = 20},
	["Turban of Elsweyr (Mask, Brown)"] = {["id"] = "WAR_KHAJIIT_TURBAN01x", ["cost"] = 20},
	["Turban of Elsweyr (Mask, White)"] = {["id"] = "WAR_KHAJIIT_TURBAN02x", ["cost"] = 20},
	["Mask of Elsweyr (Brown)"] = {["id"] = "WAR_KHAJIIT_MASK01", ["cost"] = 20},
	["Mask of Elsweyr (White)"] = {["id"] = "WAR_KHAJIIT_MASK02", ["cost"] = 20},
	}

local shirts = {
	["Twilight Cape"] = {["id"] = "WAR_BARENZIAH_CAPE2", ["cost"] = 120}, 
	["Expensive Shirt 4a"] = {["id"] = "WAR_MISC_SHIRT01a", ["cost"] = 25},
	["Expensive Shirt 4b"] = {["id"] = "WAR_MISC_SHIRT01b", ["cost"] = 25},
	["Expensive Shirt 4c"] = {["id"] = "WAR_MISC_SHIRT01c", ["cost"] = 25},
	["Expensive Shirt 4d"] = {["id"] = "WAR_MISC_SHIRT01d", ["cost"] = 25},
	["Expensive Shirt 4e"] = {["id"] = "WAR_MISC_SHIRT01e", ["cost"] = 25},
	["Fine Shirt 1a"] = {["id"] = "WAR_MISC_SHIRT02a", ["cost"] = 45},
	["Fine Shirt 1b"] = {["id"] = "WAR_MISC_SHIRT02b", ["cost"] = 45},
	["Fine Shirt 2a"] = {["id"] = "WAR_MISC_SHIRT03a", ["cost"] = 45},
	["Fine Shirt 2b"] = {["id"] = "WAR_MISC_SHIRT03b", ["cost"] = 45},
	["Fine Shirt 3a"] = {["id"] = "WAR_MISC_SHIRT04a", ["cost"] = 45},
	["Fine Shirt 3b"] = {["id"] = "WAR_MISC_SHIRT04b", ["cost"] = 45},
	["Fine Shirt 4a"] = {["id"] = "WAR_MISC_SHIRT05a", ["cost"] = 45},
	["Fine Shirt 4b"] = {["id"] = "WAR_MISC_SHIRT05b", ["cost"] = 45},
	["Fine Shirt 5a"] = {["id"] = "WAR_MISC_SHIRT06b", ["cost"] = 45},
	["Fine Shirt 5b"] = {["id"] = "WAR_MISC_SHIRT06b", ["cost"] = 45},
	["Fine Ashlander Shirt 1"] = {["id"] = "WAR_MISC_SHIRT07a", ["cost"] = 45},
	["Fine Ashlander Shirt 2"] = {["id"] = "WAR_MISC_SHIRT07b", ["cost"] = 45},
	["Fine Shirt 6a"] = {["id"] = "WAR_MISC_SHIRT08a", ["cost"] = 45},
	["Fine Shirt 6b"] = {["id"] = "WAR_MISC_SHIRT08b", ["cost"] = 45},
	["Fine Shirt 7a"] = {["id"] = "WAR_MISC_SHIRT09a", ["cost"] = 45},
	["Fine Shirt 7b"] = {["id"] = "WAR_MISC_SHIRT09b", ["cost"] = 45},
	["Blacksmith Shirt (White)"] = {["id"] = "WAR_BLACKSMITH_SHIRT01", ["cost"] = 8},
	["Blacksmith Shirt (Soot Stained)"] = {["id"] = "WAR_BLACKSMITH_SHIRT01a", ["cost"] = 8},
	["Blacksmith Shirt (Brown)"] = {["id"] = "WAR_BLACKSMITH_SHIRT01b", ["cost"] = 8},
	["Blacksmith Shirt (Green)"] = {["id"] = "WAR_BLACKSMITH_SHIRT01c", ["cost"] = 8},
	["Blacksmith Shirt (Blue)"] = {["id"] = "WAR_BLACKSMITH_SHIRT01d", ["cost"] = 8},
	["Blacksmith Shirt (Red)"] = {["id"] = "WAR_BLACKSMITH_SHIRT01e", ["cost"] = 8},
	["Fine Shirt 8a"] = {["id"] = "WAR_MISC_SHIRT10a", ["cost"] = 50},
	["Fine Shirt 8b"] = {["id"] = "WAR_MISC_SHIRT10b", ["cost"] = 50},
	["Fine Shirt 8c"] = {["id"] = "WAR_MISC_SHIRT10c", ["cost"] = 50},
	["Fine Shirt 8d"] = {["id"] = "WAR_MISC_SHIRT10d", ["cost"] = 50},
	["Fine Shirt 9a"] = {["id"] = "WAR_MISC_SHIRT11a", ["cost"] = 45},
	["Fine Shirt 9b"] = {["id"] = "WAR_MISC_SHIRT11b", ["cost"] = 45},
	["Fine Shirt 9c"] = {["id"] = "WAR_MISC_SHIRT11c", ["cost"] = 45},
	["Fine Shirt 9d"] = {["id"] = "WAR_MISC_SHIRT11d", ["cost"] = 45},
	["Fine Shirt 9e"] = {["id"] = "WAR_MISC_SHIRT11e", ["cost"] = 45},
	["Fine Shirt 9f"] = {["id"] = "WAR_MISC_SHIRT11f", ["cost"] = 45},
	["Fine Shirt 9g"] = {["id"] = "WAR_MISC_SHIRT11g", ["cost"] = 45},
	["Fine Shirt 9h"] = {["id"] = "WAR_MISC_SHIRT11h", ["cost"] = 45},
	["Common Vested Shirt a"] = {["id"] = "WAR_MISC_SHIRT12a", ["cost"] = 8},
	["Common Vested Shirt b"] = {["id"] = "WAR_MISC_SHIRT12b", ["cost"] = 8},
	["Common Vested Shirt c"] = {["id"] = "WAR_MISC_SHIRT12c", ["cost"] = 8},
	["Common Vested Shirt d"] = {["id"] = "WAR_MISC_SHIRT12d", ["cost"] = 8},
	["Common Vested Shirt e"] = {["id"] = "WAR_MISC_SHIRT12e", ["cost"] = 8},
	["Common Vested Shirt f"] = {["id"] = "WAR_MISC_SHIRT12f", ["cost"] = 8},
	["Common Vested Shirt g"] = {["id"] = "WAR_MISC_SHIRT12g", ["cost"] = 8},
	["Common Vested Shirt h"] = {["id"] = "WAR_MISC_SHIRT12h", ["cost"] = 8}
}

local pants = {
	["Blacksmith Apron/Pants 1 (Brown)"] = {["id"] = "WAR_BLACKSMITH_APRON01", ["cost"] = 8},
	["Blacksmith Apron/Pants 1 (Grey)"] = {["id"] = "WAR_BLACKSMITH_APRON01a", ["cost"] = 8},
	["Blacksmith Apron/Pants 2 (Red)"] = {["id"] = "WAR_BLACKSMITH_APRON02", ["cost"] = 8},
	["Blacksmith Apron/Pants 2 (Grey)"] = {["id"] = "WAR_BLACKSMITH_APRON02a", ["cost"] = 8},
	["Expensive Pants 4"] = {["id"] = "WAR_MISC_TRSRS_01a", ["cost"] = 20},
	["Fine Pants 1a"] = {["id"] = "WAR_MISC_TRSRS_02a", ["cost"] = 45},
	["Fine Pants 1b"] = {["id"] = "WAR_MISC_TRSRS_02b", ["cost"] = 45},
	["Fine Pants 1c"] = {["id"] = "WAR_MISC_TRSRS_02c", ["cost"] = 45},
	["Fine Pants 1d"] = {["id"] = "WAR_MISC_TRSRS_02d", ["cost"] = 45},
	["Fine Pants 2a"] = {["id"] = "WAR_MISC_TRSRS_03a", ["cost"] = 45},
	["Fine Pants 2b"] = {["id"] = "WAR_MISC_TRSRS_03b", ["cost"] = 45},
	["Fine Pants 2c"] = {["id"] = "WAR_MISC_TRSRS_03c", ["cost"] = 45},
	["Fine Pants 2d"] = {["id"] = "WAR_MISC_TRSRS_03d", ["cost"] = 45},
	["Fine Pants 2e"] = {["id"] = "WAR_MISC_TRSRS_03e", ["cost"] = 45},
	["Fine Pants 2f"] = {["id"] = "WAR_MISC_TRSRS_03f", ["cost"] = 45}
}

local robes = {
	["Noble Robes"] = {["id"] = "WAR_BARENZIAH_ROBE01", ["cost"] = 120},
	["Twilight Robes"] = {["id"] = "WAR_BARENZIAH_ROBE02", ["cost"] = 125},
	["Crimson Countess Robes"] = {["id"] = "WAR_BARENZIAH_ROBE03", ["cost"] = 90},
	["Expensive Robe of Hammerfell (White/Brown 1)"] = {["id"] = "WAR_RDGRD_ROBE_01", ["cost"] = 15},
	["Expensive Robe of Hammerfell (Cream/Green)"] = {["id"] = "WAR_RDGRD_ROBE_01x", ["cost"] = 15},
	["Expensive Robe of Hammerfell (White/Brown 2)"] = {["id"] = "WAR_RDGRD_ROBE_A", ["cost"] = 15},
	["Expensive Robe of Hammerfell (White/Blue)"] = {["id"] = "WAR_RDGRD_ROBE_Ax", ["cost"] = 15},
	["Expensive Robe of Hammerfell (White/Grey 1)"] = {["id"] = "WAR_RDGRD_ROBE_B", ["cost"] = 15},
	["Expensive Robe of Hammerfell (White/Light Blue)"] = {["id"] = "WAR_RDGRD_ROBE_Bx", ["cost"] = 15},
	["Expensive Robe of Hammerfell (White/Brown 3)"] = {["id"] = "WAR_RDGRD_ROBE_C", ["cost"] = 15},
	["Expensive Robe of Hammerfell (White/Grey 2)"] = {["id"] = "WAR_RDGRD_ROBE_Cx", ["cost"] = 15},
	["Traveller's Robes"] = {["id"] = "WAR_MLSND_ROBE_01", ["cost"] = 15},
	["Traveller's Robes (Backpack)"] = {["id"] = "WAR_MLSND_ROBE_01x", ["cost"] = 17},
	["Cosmopolitan Robes (Blue)"] = {["id"] = "WAR_MLSND_ROBE_02", ["cost"] = 15},
	["Cosmopolitan Robes (Green)"] = {["id"] = "WAR_MLSND_ROBE_02x", ["cost"] = 15},
	["Cosmopolitan Robes (Brown)"] = {["id"] = "WAR_MLSND_ROBE_02y", ["cost"] = 15},
	["Cosmopolitan Robes (Red)"] = {["id"] = "WAR_MLSND_ROBE_02z", ["cost"] = 15},
	["Robes of Elsweyr (Filigree 1)"] = {["id"] = "WAR_KHAJIIT_ROBE_01a", ["cost"] = 15},
	["Robes of Elsweyr (Filigree 2)"] = {["id"] = "WAR_KHAJIIT_ROBE_01ax", ["cost"] = 15},
	["Robes of Elsweyr (Striped 1)"] = {["id"] = "WAR_KHAJIIT_ROBE_01b", ["cost"] = 15},
	["Robes of Elsweyr (Striped 2)"] = {["id"] = "WAR_KHAJIIT_ROBE_01bx", ["cost"] = 15},
	["Robes of Elsweyr (Tapestry 1)"] = {["id"] = "WAR_KHAJIIT_ROBE_01c", ["cost"] = 15},
	["Robes of Elsweyr (Tapestry 2)"] = {["id"] = "WAR_KHAJIIT_ROBE_01cx", ["cost"] = 15},
	["Robes of Elsweyr (Quilted 1)"] = {["id"] = "WAR_KHAJIIT_ROBE_01d", ["cost"] = 15},
	["Robes of Elsweyr (Quilted 2)"] = {["id"] = "WAR_KHAJIIT_ROBE_01dx", ["cost"] = 15},
	["Robes of Elsweyr (Padded 1)"] = {["id"] = "WAR_KHAJIIT_ROBE_01e", ["cost"] = 15},
	["Robes of Elsweyr (Padded 2)"] = {["id"] = "WAR_KHAJIIT_ROBE_01ex", ["cost"] = 15},
	["Robes of Elsweyr (Dusted 1)"] = {["id"] = "WAR_KHAJIIT_ROBE_01f", ["cost"] = 15},
	["Robes of Elsweyr (Dusted 2)"] = {["id"] = "WAR_KHAJIIT_ROBE_01fx", ["cost"] = 15},
	["Robes of Elsweyr 1"] = {["id"] = "WAR_KHAJIIT_ROBE_01g", ["cost"] = 15},
	["Robes of Elsweyr 2"] = {["id"] = "WAR_KHAJIIT_ROBE_01gx", ["cost"] = 15},
	["Knight of Akatosh Armored Robe"] = {["id"] = "WAR_IMPERIAL_SRCT_Robe01", ["cost"] = 40},
	["Common Robe of Summerset (Blue)"] = {["id"] = "WAR_HIGHELF_ROBE_01a", ["cost"] = 15},
	["Common Tunic of Summerset (Blue)"] = {["id"] = "WAR_HIGHELF_ROBE_01ax", ["cost"] = 15},
	["Common Robe of Summerset (Green)"] = {["id"] = "WAR_HIGHELF_ROBE_01b", ["cost"] = 15},
	["Common Tunic of Summerset (Green)"] = {["id"] = "WAR_HIGHELF_ROBE_01bx", ["cost"] = 15},
	["Common Robe of Summerset (Purple)"] = {["id"] = "WAR_HIGHELF_ROBE_01c", ["cost"] = 15},
	["Common Tunic of Summerset (Purple)"] = {["id"] = "WAR_HIGHELF_ROBE_01cx", ["cost"] = 15},
	["Common Robe of Summerset (Brown)"] = {["id"] = "WAR_HIGHELF_ROBE_01d", ["cost"] = 15},
	["Common Tunic of Summerset (Brown)"] = {["id"] = "WAR_HIGHELF_ROBE_01dx", ["cost"] = 15},
	["Fine Robe of Summerset (White)"] = {["id"] = "WAR_HIGHELF_ROBE_02a", ["cost"] = 25},
	["Fine Tunic of Summerset (White)"] = {["id"] = "WAR_HIGHELF_ROBE_02ax", ["cost"] = 25},
	["Fine Robe of Summerset (Blue)"] = {["id"] = "WAR_HIGHELF_ROBE_02b", ["cost"] = 25},
	["Fine Tunic of Summerset (Blue)"] = {["id"] = "WAR_HIGHELF_ROBE_02bx", ["cost"] = 25},
	["Fine Robe of Summerset (Red)"] = {["id"] = "WAR_HIGHELF_ROBE_02c", ["cost"] = 25},
	["Fine Tunic of Summerset (Red)"] = {["id"] = "WAR_HIGHELF_ROBE_02cx", ["cost"] = 25},
	["Fine Robe of Summerset (Green)"] = {["id"] = "WAR_HIGHELF_ROBE_02d", ["cost"] = 25},
	["Fine Tunic of Summerset (Green)"] = {["id"] = "WAR_HIGHELF_ROBE_02dx", ["cost"] = 25},
	["Exquisite Robe of Summerset (Blue)"] = {["id"] = "WAR_HIGHELF_ROBE_03a", ["cost"] = 80},
	["Exquisite Robe of Summerset (Red)"] = {["id"] = "WAR_HIGHELF_ROBE_03b", ["cost"] = 80},
	["Exquisite Robe of Summerset (Green)"] = {["id"] = "WAR_HIGHELF_ROBE_03c", ["cost"] = 80},
	["Exquisite Robe of Summerset (Purple)"] = {["id"] = "WAR_HIGHELF_ROBE_03d", ["cost"] = 80},
	["Exquisite Robe of Summerset (White 1)"] = {["id"] = "WAR_HIGHELF_ROBE_03e", ["cost"] = 80},
	["Exquisite Robe of Summerset (Black 1)"] = {["id"] = "WAR_HIGHELF_ROBE_03f", ["cost"] = 80},
	["Exquisite Robe of Summerset (Black 2)"] = {["id"] = "WAR_HIGHELF_ROBE_03g", ["cost"] = 80},
	["Exquisite Robe of Summerset (White 2)"] = {["id"] = "WAR_HIGHELF_ROBE_03h", ["cost"] = 80},
}

local rgloves = {
	["Wrist Wrap of Elsewyr R 1"] = {["id"] = "WAR_KHAJIIT_WRSTWRP01_R", ["cost"] = 15},
	["Wrist Wrap of Elsewyr R 2"] = {["id"] = "WAR_KHAJIIT_WRSTWRP02_R", ["cost"] = 15},
}

local lgloves = {
	["Wrist Wrap of Elsewyr L 1"] = {["id"] = "WAR_KHAJIIT_WRSTWRP01_L", ["cost"] = 15},
	["Wrist Wrap of Elsewyr L 2"] = {["id"] = "WAR_KHAJIIT_WRSTWRP02_L", ["cost"] = 15},
}

local shoes = {
	["Noble Boots"] = {["id"] = "WAR_BARENZIAH_SHOE01", ["cost"] = 80},
	["Twilight Shoes"] = {["id"] = "WAR_BARENZIAH_SHOE02", ["cost"] = 80},
	["Crimson Countess Boots"] = {["id"] = "WAR_BARENZIAH_SHOE03", ["cost"] = 80},
	["Travellers Shoes"] = {["id"] = "WAR_MLSND_BOOT_01", ["cost"] = 10},
	["Cosmopolitan Boots"] = {["id"] = "WAR_MLSND_BOOT_02", ["cost"] = 15},
	["Common Boots of Summerset 1"] = {["id"] = "WAR_HIGHELF_BOOT_01a", ["cost"] = 6},
	["Common Boots of Summerset 2"] = {["id"] = "WAR_HIGHELF_BOOT_01b", ["cost"] = 6},
	["Common Boots of Summerset 3"] = {["id"] = "WAR_HIGHELF_BOOT_01c", ["cost"] = 6},
	["Common Boots of Summerset 4"] = {["id"] = "WAR_HIGHELF_BOOT_01d", ["cost"] = 6},
	["Fine Boots of Summerset 1"] = {["id"] = "WAR_HIGHELF_BOOT_02a", ["cost"] = 12},
	["Fine Boots of Summerset 2"] = {["id"] = "WAR_HIGHELF_BOOT_02b", ["cost"] = 12},
	["Fine Boots of Summerset 3"] = {["id"] = "WAR_HIGHELF_BOOT_02c", ["cost"] = 12},
	["Fine Boots of Summerset 4"] = {["id"] = "WAR_HIGHELF_BOOT_02d", ["cost"] = 12},
	["Fine Shoes 2a"] = {["id"] = "WAR_MISC_SHOES_02a", ["cost"] = 20},
	["Fine Shoes 2b"] = {["id"] = "WAR_MISC_SHOES_02b", ["cost"] = 20},
	["Fine Shoes 3a"] = {["id"] = "WAR_MISC_SHOES_03a", ["cost"] = 20},
	["Fine Shoes 3b"] = {["id"] = "WAR_MISC_SHOES_03b", ["cost"] = 20},
	["Fine Shoes 4a"] = {["id"] = "WAR_MISC_SHOES_04a", ["cost"] = 20},
	["Fine Shoes 4b"] = {["id"] = "WAR_MISC_SHOES_04b", ["cost"] = 20},
	["Fine Shoes 5a"] = {["id"] = "WAR_MISC_SHOES_05a", ["cost"] = 20},
	["Fine Shoes 5b"] = {["id"] = "WAR_MISC_SHOES_05b", ["cost"] = 20},
	["Fine Shoes 6a"] = {["id"] = "WAR_MISC_SHOES_06a", ["cost"] = 20},
	["Fine Shoes 6b"] = {["id"] = "WAR_MISC_SHOES_06b", ["cost"] = 20},
	["Fine Shoes 8a"] = {["id"] = "WAR_MISC_SHOES_08a", ["cost"] = 20},
	["Fine Shoes 8b"] = {["id"] = "WAR_MISC_SHOES_08b", ["cost"] = 20},
	["Fine Shoes 9a"] = {["id"] = "WAR_MISC_SHOES_09a", ["cost"] = 20},
	["Fine Shoes 9b"] = {["id"] = "WAR_MISC_SHOES_09b", ["cost"] = 20},
	["Fine Boots 1a"] = {["id"] = "WAR_MISC_BOOTS_01a", ["cost"] = 45},
	["Fine Boots 1b"] = {["id"] = "WAR_MISC_BOOTS_01b", ["cost"] = 45},
	["Fine Ashlander Boots"] = {["id"] = "WAR_MISC_BOOTS_01c", ["cost"] = 45},
	["Fine Boots 1"] = {["id"] = "WAR_MISC_BOOTS_01d", ["cost"] = 45},
	["Fine Boots 2"] = {["id"] = "WAR_MISC_BOOTS_01e", ["cost"] = 45},
	["Fine Slipper Sandal 2a"] = {["id"] = "WAR_MISC_SLPPR_02a", ["cost"] = 12},
	["Fine Slipper Sandal 2b"] = {["id"] = "WAR_MISC_SLPPR_02b", ["cost"] = 12},
	["Fine Slipper Sandal 3a"] = {["id"] = "WAR_MISC_SLPPR_03a", ["cost"] = 12},
	["Fine Slipper Sandal 3b"] = {["id"] = "WAR_MISC_SLPPR_03b", ["cost"] = 12},
	["Fine Slipper Sandal 4a"] = {["id"] = "WAR_MISC_SLPPR_04a", ["cost"] = 12},
	["Fine Slipper Sandal 4b"] = {["id"] = "WAR_MISC_SLPPR_04b", ["cost"] = 12},
	["Fine Slipper Sandal 5a"] = {["id"] = "WAR_MISC_SLPPR_05a", ["cost"] = 12},
	["Fine Slipper Sandal 5b"] = {["id"] = "WAR_MISC_SLPPR_05b", ["cost"] = 12},
	["Fine Slipper Sandal 6a"] = {["id"] = "WAR_MISC_SLPPR_06a", ["cost"] = 12},
	["Fine Slipper Sandal 6b"] = {["id"] = "WAR_MISC_SLPPR_06b", ["cost"] = 12},
	["Fine Slipper Sandal 8a"] = {["id"] = "WAR_MISC_SLPPR_08a", ["cost"] = 12},
	["Fine Slipper Sandal 8b"] = {["id"] = "WAR_MISC_SLPPR_08b", ["cost"] = 12},
	["Fine Slipper Sandal 9a"] = {["id"] = "WAR_MISC_SLPPR_09a", ["cost"] = 12},
	["Fine Slipper Sandal 9b"] = {["id"] = "WAR_MISC_SLPPR_09b", ["cost"] = 12},
	["Fine Thong Sandal 1"] = {["id"] = "WAR_MISC_THONG_01", ["cost"] = 12},
	["Fine Thong Sandal 2"] = {["id"] = "WAR_MISC_THONG_02", ["cost"] = 12},
	["Fine Thong Sandal 3"] = {["id"] = "WAR_MISC_THONG_03", ["cost"] = 12},
	["Fine Thong Sandal 4"] = {["id"] = "WAR_MISC_THONG_04", ["cost"] = 12},
	["Fine Thong Sandal 5"] = {["id"] = "WAR_MISC_THONG_05", ["cost"] = 12},
	["Fine Thong Sandal 6"] = {["id"] = "WAR_MISC_THONG_06", ["cost"] = 12},
	["Fine Thong Sandal 7"] = {["id"] = "WAR_MISC_THONG_07", ["cost"] = 12},
	["Fine Shoes 10a"] = {["id"] = "WAR_MISC_SHOES_10a", ["cost"] = 30},
	["Fine Shoes 10b"] = {["id"] = "WAR_MISC_SHOES_10b", ["cost"] = 30},
	["Fine Shoes 10c"] = {["id"] = "WAR_MISC_SHOES_10c", ["cost"] = 30},
	["Fine Shoes 10d"] = {["id"] = "WAR_MISC_SHOES_10d", ["cost"] = 30},
	["Fine Shoes Padded 11a"] = {["id"] = "WAR_MISC_SHOES_11a", ["cost"] = 20},
	["Fine Shoes Padded 11b"] = {["id"] = "WAR_MISC_SHOES_11b", ["cost"] = 20},
	["Fine Shoes Padded 11c"] = {["id"] = "WAR_MISC_SHOES_11c", ["cost"] = 20},
	["Fine Shoes Padded 11d"] = {["id"] = "WAR_MISC_SHOES_11d", ["cost"] = 20},
	["Fine Shoes Padded 11e"] = {["id"] = "WAR_MISC_SHOES_11e", ["cost"] = 20},
	["Fine Shoes Padded 11f"] = {["id"] = "WAR_MISC_SHOES_11f", ["cost"] = 20},
	["Fine Shoes Padded 11g"] = {["id"] = "WAR_MISC_SHOES_11g", ["cost"] = 20},
	["Fine Shoes Padded 11h"] = {["id"] = "WAR_MISC_SHOES_11h", ["cost"] = 20},
	["Exquisite Boots of Summerset 3a"] = {["id"] = "WAR_HIGHELF_BOOT_03a", ["cost"] = 80},
	["Exquisite Boots of Summerset 3b"] = {["id"] = "WAR_HIGHELF_BOOT_03b", ["cost"] = 80}
}

local heads = {
	["wood elf"] = {
		["Liria"] = {["id"] = "WAR_RT_WEx_LIRIA"},
	}
}

local hairs = {
	["wood elf"] = {
		["Liria Hair"] = {["id"] = "WAR_RT_WEx_LIRIA_hr"},
	}
}

local function OnServerPostInitHandler()
	for entryName,entry in pairs(helmets) do
		jrpShop.config.helmets[entryName] = entry
	end

	for entryName,entry in pairs(shirts) do
		jrpShop.config.shirts[entryName] = entry
	end

	for entryName,entry in pairs(pants) do
		jrpShop.config.pants[entryName] = entry
	end

	for entryName,entry in pairs(robes) do
		jrpShop.config.robes[entryName] = entry
	end
	
	for entryName,entry in pairs(rgloves) do
		jrpShop.config.rgloves[entryName] = entry
	end
	
	for entryName,entry in pairs(lgloves) do
		jrpShop.config.lgloves[entryName] = entry
	end
	
	for entryName,entry in pairs(shoes) do
		jrpShop.config.shoes[entryName] = entry
	end
	
	for entryName,entry in pairs(heads) do
		for subentryName,subentry in pairs(entry) do
			jrpShop.config.heads[entryName][subentryName] = subentry
		end
	end
	
	for entryName,entry in pairs(hairs) do
		for subentryName,subentry in pairs(entry) do
			jrpShop.config.hairs[entryName][subentryName] = subentry
		end
	end
	
	DataManager.saveConfiguration("jrpShop",jrpShop.config)
	tes3mp.LogMessage(2, "[jrpShop] Loaded addon - FineClothiersOfTamriel")
end

customEventHooks.registerHandler("OnServerPostInit", OnServerPostInitHandler)

