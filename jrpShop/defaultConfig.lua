-- defaultConfig for jrpShop. Should contain all Morrowind+Tribunal+Bloodmoon non-unique items (very lightly skimmed over)
local defaultConfig = {
	heads = {
		["cost"] = 500,
		["argonian"] = {
			["F Head 1"] = {["id"] = "b_n_argonian_f_head_01"},
			["F Head 2"] = {["id"] = "b_n_argonian_f_head_02"},
			["F Head 3"] = {["id"] = "b_n_argonian_f_head_03"},
			["F Vampire 1"] = {["id"] = "b_v_argonian_f_head_01"},
			["M Head 1"] = {["id"] = "b_n_argonian_m_head_01"},
			["M Head 2"] = {["id"] = "b_n_argonian_m_head_02"},
			["M Head 3"] = {["id"] = "b_n_argonian_m_head_03"},
			["M Vampire 1"] = {["id"] = "b_v_argonian_m_head_01"}
		},
		["breton"] = {
			["F Head 1"] = {["id"] = "b_n_breton_f_head_01"},
			["F Head 2"] = {["id"] = "b_n_breton_f_head_02"},
			["F Head 3"] = {["id"] = "b_n_breton_f_head_03"},
			["F Head 4"] = {["id"] = "b_n_breton_f_head_04"},
			["F Head 5"] = {["id"] = "b_n_breton_f_head_05"},
			["F Head 6"] = {["id"] = "b_n_breton_f_head_06"},
			["F Vampire 1"] = {["id"] = "b_v_breton_f_head_01"},
			["M Head 1"] = {["id"] = "b_n_breton_m_head_01"},
			["M Head 2"] = {["id"] = "b_n_breton_m_head_02"},
			["M Head 3"] = {["id"] = "b_n_breton_m_head_03"},
			["M Head 4"] = {["id"] = "b_n_breton_m_head_04"},
			["M Head 5"] = {["id"] = "b_n_breton_m_head_05"},
			["M Head 6"] = {["id"] = "b_n_breton_m_head_06"},
			["M Head 7"] = {["id"] = "b_n_breton_m_head_07"},
			["M Head 8"] = {["id"] = "b_n_breton_m_head_08"},
			["M Vampire 1"] = {["id"] = "b_v_breton_m_head_01"}
		},
		["dark elf"] = {
			["F Barenziah"] = {["id"] = "b_n_dark elf_f_barenziah"},
			["F Head 1"] = {["id"] = "b_n_dark elf_f_head_01"},
			["F Head 2"] = {["id"] = "b_n_dark elf_f_head_02"},
			["F Head 3"] = {["id"] = "b_n_dark elf_f_head_03"},
			["F Head 4"] = {["id"] = "b_n_dark elf_f_head_04"},
			["F Head 5"] = {["id"] = "b_n_dark elf_f_head_05"},
			["F Head 6"] = {["id"] = "b_n_dark elf_f_head_06"},
			["F Head 7"] = {["id"] = "b_n_dark elf_f_head_07"},
			["F Head 8"] = {["id"] = "b_n_dark elf_f_head_08"},
			["F Head 9"] = {["id"] = "b_n_dark elf_f_head_09"},
			["F Head 10"] = {["id"] = "b_n_dark elf_f_head_10"},
			["F Vampire 1"] = {["id"] = "b_v_dark elf_f_head_01"},
			["M Head 1"] = {["id"] = "b_n_dark elf_m_head_01"},
			["M Head 2"] = {["id"] = "b_n_dark elf_m_head_02"},
			["M Head 3"] = {["id"] = "b_n_dark elf_m_head_03"},
			["M Head 4"] = {["id"] = "b_n_dark elf_m_head_04"},
			["M Head 5"] = {["id"] = "b_n_dark elf_m_head_05"},
			["M Head 6"] = {["id"] = "b_n_dark elf_m_head_06"},
			["M Head 7"] = {["id"] = "b_n_dark elf_m_head_07"},
			["M Head 8"] = {["id"] = "b_n_dark elf_m_head_08"},
			["M Head 9"] = {["id"] = "b_n_dark elf_m_head_09"},
			["M Head 10"] = {["id"] = "b_n_dark elf_m_head_10"},
			["M Head 11"] = {["id"] = "b_n_dark elf_m_head_11"},
			["M Head 12"] = {["id"] = "b_n_dark elf_m_head_12"},
			["M Head 13"] = {["id"] = "b_n_dark elf_m_head_13"},
			["M Head 14"] = {["id"] = "b_n_dark elf_m_head_14"},
			["M Head 15"] = {["id"] = "b_n_dark elf_m_head_15"},
			["M Head 16"] = {["id"] = "b_n_dark elf_m_head_16"},
			["M Head 17"] = {["id"] = "b_n_dark elf_m_head_17"},
			["M Helseth"] = {["id"] = "b_n_dark elf_m_helseth"},
			["M Vampire 1"] = {["id"] = "b_v_dark elf_m_head_01"}
		},
		["high elf"] = {
			["F Head 1"] = {["id"] = "b_n_high elf_f_head_01"},
			["F Head 2"] = {["id"] = "b_n_high elf_f_head_02"},
			["F Head 3"] = {["id"] = "b_n_high elf_f_head_03"},
			["F Head 4"] = {["id"] = "b_n_high elf_f_head_04"},
			["F Head 5"] = {["id"] = "b_n_high elf_f_head_05"},
			["F Head 6"] = {["id"] = "b_n_high elf_f_head_06"},
			["F Vampire 1"] = {["id"] = "b_v_high elf_f_head_01"},
			["M Head 1"] = {["id"] = "b_n_high elf_m_head_01"},
			["M Head 2"] = {["id"] = "b_n_high elf_m_head_02"},
			["M Head 3"] = {["id"] = "b_n_high elf_m_head_03"},
			["M Head 4"] = {["id"] = "b_n_high elf_m_head_04"},
			["M Head 5"] = {["id"] = "b_n_high elf_m_head_05"},
			["M Head 6"] = {["id"] = "b_n_high elf_m_head_06"},
			["M Vampire 1"] = {["id"] = "b_v_high elf_m_head_01"}
		},
		["imperial"] = {
			["F Head 1"] = {["id"] = "b_n_imperial_f_head_01"},
			["F Head 2"] = {["id"] = "b_n_imperial_f_head_02"},
			["F Head 3"] = {["id"] = "b_n_imperial_f_head_03"},
			["F Head 4"] = {["id"] = "b_n_imperial_f_head_04"},
			["F Head 5"] = {["id"] = "b_n_imperial_f_head_05"},
			["F Head 6"] = {["id"] = "b_n_imperial_f_head_06"},
			["F Head 7"] = {["id"] = "b_n_imperial_f_head_07"},
			["F Vampire 1"] = {["id"] = "b_v_imperial_f_head_01"},
			["M Head 1"] = {["id"] = "b_n_imperial_m_head_01"},
			["M Head 2"] = {["id"] = "b_n_imperial_m_head_02"},
			["M Head 3"] = {["id"] = "b_n_imperial_m_head_03"},
			["M Head 4"] = {["id"] = "b_n_imperial_m_head_04"},
			["M Head 5"] = {["id"] = "b_n_imperial_m_head_05"},
			["M Head 6"] = {["id"] = "b_n_imperial_m_head_06"},
			["M Head 7"] = {["id"] = "b_n_imperial_m_head_07"},
			["M Vampire 1"] = {["id"] = "b_v_imperial_m_head_01"}
		},
		["khajiit"] = {
			["F Head 1"] = {["id"] = "b_n_khajiit_f_head_01"},
			["F Head 2"] = {["id"] = "b_n_khajiit_f_head_02"},
			["F Head 3"] = {["id"] = "b_n_khajiit_f_head_03"},
			["F Head 4"] = {["id"] = "b_n_khajiit_f_head_04"},
			["F Vampire 1"] = {["id"] = "b_v_khajiit_f_head_01"},
			["M Head 1"] = {["id"] = "b_n_khajiit_m_head_01"},
			["M Head 2"] = {["id"] = "b_n_khajiit_m_head_02"},
			["M Head 3"] = {["id"] = "b_n_khajiit_m_head_03"},
			["M Head 4"] = {["id"] = "b_n_khajiit_m_head_04"},
			["M Vampire 1"] = {["id"] = "b_v_khajiit_m_head_01"}
		},
		["nord"] = {
			["F Head 1"] = {["id"] = "b_n_nord_f_head_01"},
			["F Head 2"] = {["id"] = "b_n_nord_f_head_02"},
			["F Head 3"] = {["id"] = "b_n_nord_f_head_03"},
			["F Head 4"] = {["id"] = "b_n_nord_f_head_04"},
			["F Head 5"] = {["id"] = "b_n_nord_f_head_05"},
			["F Head 6"] = {["id"] = "b_n_nord_f_head_06"},
			["F Head 7"] = {["id"] = "b_n_nord_f_head_07"},
			["F Head 8"] = {["id"] = "b_n_nord_f_head_08"},
			["F Head 9"] = {["id"] = "b_n_nord_f_head_09"}, -- bloodmoon
			["F Head 10"] = {["id"] = "b_n_nord_f_head_10"}, -- bloodmoon
			["F Head 11"] = {["id"] = "b_n_nord_f_head_11"}, -- bloodmoon
			["F Head 12"] = {["id"] = "b_n_nord_f_head_12"}, -- bloodmoon
			["F Head 13"] = {["id"] = "b_n_nord_f_head_13"}, -- bloodmoon
			["F Vampire 1"] = {["id"] = "b_v_nord_f_head_01"},
			["M Head 1"] = {["id"] = "b_n_nord_m_head_01"},
			["M Head 2"] = {["id"] = "b_n_nord_m_head_02"},
			["M Head 3"] = {["id"] = "b_n_nord_m_head_03"},
			["M Head 4"] = {["id"] = "b_n_nord_m_head_04"},
			["M Head 5"] = {["id"] = "b_n_nord_m_head_05"},
			["M Head 6"] = {["id"] = "b_n_nord_m_head_06"},
			["M Head 7"] = {["id"] = "b_n_nord_m_head_07"},
			["M Head 8"] = {["id"] = "b_n_nord_m_head_08"},
			["M Head 9"] = {["id"] = "b_n_nord_m_head_09"}, -- bloodmoon
			["M Head 10"] = {["id"] = "b_n_nord_m_head_10"}, -- bloodmoon
			["M Head 11"] = {["id"] = "b_n_nord_m_head_11"}, -- bloodmoon
			["M Head 12"] = {["id"] = "b_n_nord_m_head_12"}, -- bloodmoon
			["M Head 13"] = {["id"] = "b_n_nord_m_head_13"}, -- bloodmoon
			["M Vampire 1"] = {["id"] = "b_v_nord_m_head_01"}
		},
		["orc"] = {
			["F Head 1"] = {["id"] = "b_n_orc_f_head_01"},
			["F Head 2"] = {["id"] = "b_n_orc_f_head_02"},
			["F Head 3"] = {["id"] = "b_n_orc_f_head_03"},
			["F Vampire 1"] = {["id"] = "b_v_orc_f_head_01"},
			["M Head 1"] = {["id"] = "b_n_orc_m_head_01"},
			["M Head 2"] = {["id"] = "b_n_orc_m_head_02"},
			["M Head 3"] = {["id"] = "b_n_orc_m_head_03"},
			["M Head 4"] = {["id"] = "b_n_orc_m_head_04"},
			["M Vampire 1"] = {["id"] = "b_v_orc_m_head_01"}
		},
		["redguard"] = {
			["F Head 1"] = {["id"] = "b_n_redguard_f_head_01"},
			["F Head 2"] = {["id"] = "b_n_redguard_f_head_02"},
			["F Head 3"] = {["id"] = "b_n_redguard_f_head_03"},
			["F Head 4"] = {["id"] = "b_n_redguard_f_head_06"},
			["F Elder 1"] = {["id"] = "b_n_redguard_f_head_04"},
			["F Elder 2"] = {["id"] = "b_n_redguard_f_head_05"},
			["F Vampire 1"] = {["id"] = "b_v_redguard_f_head_01"},
			["M Head 1"] = {["id"] = "b_n_redguard_m_head_01"},
			["M Head 2"] = {["id"] = "b_n_redguard_m_head_02"},
			["M Head 3"] = {["id"] = "b_n_redguard_m_head_03"},
			["M Head 4"] = {["id"] = "b_n_redguard_m_head_04"},
			["M Head 5"] = {["id"] = "b_n_redguard_m_head_05"},
			["M Head 6"] = {["id"] = "b_n_redguard_m_head_06"},
			["M Vampire 1"] = {["id"] = "b_v_redguard_m_head_01"}
		},
		["wood elf"] = {
			["F Head 1"] = {["id"] = "b_n_wood elf_f_head_01"},
			["F Head 2"] = {["id"] = "b_n_wood elf_f_head_02"},
			["F Head 3"] = {["id"] = "b_n_wood elf_f_head_03"},
			["F Head 4"] = {["id"] = "b_n_wood elf_f_head_04"},
			["F Head 5"] = {["id"] = "b_n_wood elf_f_head_05"},
			["F Head 6"] = {["id"] = "b_n_wood elf_f_head_06"},
			["F Vampire 1"] = {["id"] = "b_v_wood elf_f_head_01"},
			["M Head 1"] = {["id"] = "b_n_wood elf_m_head_01"},
			["M Head 2"] = {["id"] = "b_n_wood elf_m_head_02"},
			["M Head 3"] = {["id"] = "b_n_wood elf_m_head_03"},
			["M Head 4"] = {["id"] = "b_n_wood elf_m_head_04"},
			["M Head 5"] = {["id"] = "b_n_wood elf_m_head_05"},
			["M Head 6"] = {["id"] = "b_n_wood elf_m_head_06"},
			["M Head 7"] = {["id"] = "b_n_wood elf_m_head_07"},
			["M Head 8"] = {["id"] = "b_n_wood elf_m_head_08"},
			["M Vampire 1"] = {["id"] = "b_v_wood elf_m_head_01"}
		}
	},
	hairs = {
		["cost"] = 0,
		["argonian"] = {
			["F Hair 1"] = {["id"] = "b_n_argonian_f_hair_01"},
			["F Hair 2"] = {["id"] = "b_n_argonian_f_hair_02"},
			["F Hair 3"] = {["id"] = "b_n_argonian_f_hair_03"},
			["F Hair 4"] = {["id"] = "b_n_argonian_f_hair_04"},
			["F Hair 5"] = {["id"] = "b_n_argonian_f_hair_05"},
			["M Hair 1"] = {["id"] = "b_n_argonian_m_hair_01"},
			["M Hair 2"] = {["id"] = "b_n_argonian_m_hair_02"},
			["M Hair 3"] = {["id"] = "b_n_argonian_m_hair_03"},
			["M Hair 4"] = {["id"] = "b_n_argonian_m_hair_04"},
			["M Hair 5"] = {["id"] = "b_n_argonian_m_hair_05"},
			["M Hair 6"] = {["id"] = "b_n_argonian_m_hair_06"}
		},
		["breton"] = {
			["F Hair 1"] = {["id"] = "b_n_breton_f_hair_01"},
			["F Hair 2"] = {["id"] = "b_n_breton_f_hair_02"},
			["F Hair 3"] = {["id"] = "b_n_breton_f_hair_03"},
			["F Hair 4"] = {["id"] = "b_n_breton_f_hair_04"},
			["F Hair 5"] = {["id"] = "b_n_breton_f_hair_05"},
			["M Hair 1"] = {["id"] = "b_n_breton_m_hair_00"},
			["M Hair 2"] = {["id"] = "b_n_breton_m_hair_01"},
			["M Hair 3"] = {["id"] = "b_n_breton_m_hair_02"},
			["M Hair 4"] = {["id"] = "b_n_breton_m_hair_03"},
			["M Hair 5"] = {["id"] = "b_n_breton_m_hair_04"},
			["M Hair 6"] = {["id"] = "b_n_breton_m_hair_05"},
		},
		["dark elf"] = {
			["F Barenziah"] = {["id"] = "b_n_dark elf_f_barenziahh"},
			["F Short w/ Sides (Black)"] = {["id"] = "b_n_dark elf_f_hair_01"},
			["F Fade (Orange)"] = {["id"] = "b_n_dark elf_f_hair_02"},
			["F Short w/ Sides (Brown)"] = {["id"] = "b_n_dark elf_f_hair_03"},
			["F Fade (Brown)"] = {["id"] = "b_n_dark elf_f_hair_04"},
			["F Tight Bun (Black)"] = {["id"] = "b_n_dark elf_f_hair_05"},
			["F Tight Bun w/ Gold Crown (Black)"] = {["id"] = "b_n_dark elf_f_hair_06"},
			["F Tight Bun (Orange)"] = {["id"] = "b_n_dark elf_f_hair_07"},
			["F Tight Bun, feathered (Black)"] = {["id"] = "b_n_dark elf_f_hair_08"},
			["F Top Bun, Tiara (Black)"] = {["id"] = "b_n_dark elf_f_hair_09"},
			["F Braided Medium w/ jewels (White)"] = {["id"] = "b_n_dark elf_f_hair_10"},
			["F Military Cut (Brown)"] = {["id"] = "b_n_dark elf_f_hair_11"},
			["F Medium w/ bangs (Brown)"] = {["id"] = "b_n_dark elf_f_hair_12"},
			["F Greased w/ ahoge (White)"] = {["id"] = "b_n_dark elf_f_hair_13"},
			["F Tall w/ tiara (Black)"] = {["id"] = "b_n_dark elf_f_hair_14"},
			["F Braided Medium (White)"] = {["id"] = "b_n_dark elf_f_hair_15"},
			["F Braided Medium (Brown)"] = {["id"] = "b_n_dark elf_f_hair_16"},
			["F Braided Medium (Orange)"] = {["id"] = "b_n_dark elf_f_hair_17"},
			["F Receeding Medium (Brown)"] = {["id"] = "b_n_dark elf_f_hair_18"},
			["F Spiked Up (Orange)"] = {["id"] = "b_n_dark elf_f_hair_19"},
			["F Top Bun (Black)"] = {["id"] = "b_n_dark elf_f_hair_20"},
			["F Spiked Up (Brown)"] = {["id"] = "b_n_dark elf_f_hair_21"},
			["F Receeding Ponytail (Gray)"] = {["id"] = "b_n_dark elf_f_hair_22"},
			["F Receeding Ponytail (White)"] = {["id"] = "b_n_dark elf_f_hair_23"},
			["F Straight Shoulder-Length (Orange)"] = {["id"] = "b_n_dark elf_f_hair_24"},
			["M Helseth"] = {["id"] = "b_n_dark elf_m_helsethHair"},
			["M Hair 1"] = {["id"] = "b_n_dark elf_m_hair_01"},
			["M Hair 2"] = {["id"] = "b_n_dark elf_m_hair_02"},
			["M Hair 3"] = {["id"] = "b_n_dark elf_m_hair_03"},
			["M Hair 4"] = {["id"] = "b_n_dark elf_m_hair_04"},
			["M Hair 5"] = {["id"] = "b_n_dark elf_m_hair_05"},
			["M Hair 6"] = {["id"] = "b_n_dark elf_m_hair_06"},
			["M Hair 7"] = {["id"] = "b_n_dark elf_m_hair_07"},
			["M Hair 8"] = {["id"] = "b_n_dark elf_m_hair_08"},
			["M Hair 9"] = {["id"] = "b_n_dark elf_m_hair_09"},
			["M Hair 10"] = {["id"] = "b_n_dark elf_m_hair_10"},
			["M Hair 11"] = {["id"] = "b_n_dark elf_m_hair_11"},
			["M Hair 12"] = {["id"] = "b_n_dark elf_m_hair_12"},
			["M Hair 13"] = {["id"] = "b_n_dark elf_m_hair_13"},
			["M Hair 14"] = {["id"] = "b_n_dark elf_m_hair_14"},
			["M Hair 15"] = {["id"] = "b_n_dark elf_m_hair_15"},
			["M Hair 16"] = {["id"] = "b_n_dark elf_m_hair_16"},
			["M Hair 17"] = {["id"] = "b_n_dark elf_m_hair_17"},
			["M Hair 18"] = {["id"] = "b_n_dark elf_m_hair_18"},
			["M Hair 19"] = {["id"] = "b_n_dark elf_m_hair_19"},
			["M Hair 20"] = {["id"] = "b_n_dark elf_m_hair_20"},
			["M Hair 21"] = {["id"] = "b_n_dark elf_m_hair_21"},
			["M Hair 22"] = {["id"] = "b_n_dark elf_m_hair_22"},
			["M Hair 23"] = {["id"] = "b_n_dark elf_m_hair_23"},
			["M Hair 24"] = {["id"] = "b_n_dark elf_m_hair_24"},
			["M Hair 25"] = {["id"] = "b_n_dark elf_m_hair_25"},
			["M Hair 26"] = {["id"] = "b_n_dark elf_m_hair_26"}
		},
		["high elf"] = {
			["F Hair 1"] = {["id"] = "b_n_high elf_f_hair_01"},
			["F Hair 2"] = {["id"] = "b_n_high elf_f_hair_02"},
			["F Hair 3"] = {["id"] = "b_n_high elf_f_hair_03"},
			["F Hair 4"] = {["id"] = "b_n_high elf_f_hair_04"},
			["M Hair 1"] = {["id"] = "b_n_high elf_m_hair_01"},
			["M Hair 2"] = {["id"] = "b_n_high elf_m_hair_02"},
			["M Hair 3"] = {["id"] = "b_n_high elf_m_hair_03"},
			["M Hair 4"] = {["id"] = "b_n_high elf_m_hair_04"},
			["M Hair 5"] = {["id"] = "b_n_high elf_m_hair_05"},
		},
		["imperial"] = {
			["F Hair 1"] = {["id"] = "b_n_imperial_f_hair_01"},
			["F Hair 2"] = {["id"] = "b_n_imperial_f_hair_02"},
			["F Hair 3"] = {["id"] = "b_n_imperial_f_hair_03"},
			["F Hair 4"] = {["id"] = "b_n_imperial_f_hair_04"},
			["F Hair 5"] = {["id"] = "b_n_imperial_f_hair_05"},
			["F Hair 6"] = {["id"] = "b_n_imperial_f_hair_06"},
			["F Hair 7"] = {["id"] = "b_n_imperial_f_hair_07"},
			["M Hair 1"] = {["id"] = "b_n_imperial_m_hair_01"},
			["M Hair 2"] = {["id"] = "b_n_imperial_m_hair_02"},
			["M Hair 3"] = {["id"] = "b_n_imperial_m_hair_03"},
			["M Hair 4"] = {["id"] = "b_n_imperial_m_hair_04"},
			["M Hair 5"] = {["id"] = "b_n_imperial_m_hair_05"},
			["M Hair 6"] = {["id"] = "b_n_imperial_m_hair_06"},
			["M Hair 7"] = {["id"] = "b_n_imperial_m_hair_07"},
			["M Hair 8"] = {["id"] = "b_n_imperial_m_hair_08"},
			["M Hair 9"] = {["id"] = "b_n_imperial_m_hair_09"},
		},
		["khajiit"] = {
			["F Hair 1"] = {["id"] = "b_n_khajiit_f_hair_01"},
			["F Hair 2"] = {["id"] = "b_n_khajiit_f_hair_02"},
			["F Hair 3"] = {["id"] = "b_n_khajiit_f_hair_03"},
			["F Hair 4"] = {["id"] = "b_n_khajiit_f_hair_04"},
			["F Hair 5"] = {["id"] = "b_n_khajiit_f_hair_05"},
			["M Hair 1"] = {["id"] = "b_n_khajiit_m_hair_01"},
			["M Hair 2"] = {["id"] = "b_n_khajiit_m_hair_02"},
			["M Hair 3"] = {["id"] = "b_n_khajiit_m_hair_03"},
			["M Hair 4"] = {["id"] = "b_n_khajiit_m_hair_04"},
			["M Hair 5"] = {["id"] = "b_n_khajiit_m_hair_05"},
		},
		["nord"] = {
			["F Hair 1"] = {["id"] = "b_n_nord_f_hair_01"},
			["F Hair 2"] = {["id"] = "b_n_nord_f_hair_02"},
			["F Hair 3"] = {["id"] = "b_n_nord_f_hair_03"},
			["F Hair 4"] = {["id"] = "b_n_nord_f_hair_04"},
			["F Hair 5"] = {["id"] = "b_n_nord_f_hair_05"},
			["F Hair 6"] = {["id"] = "b_n_nord_f_hair_06"}, -- bloodmoon
			["M Hair 1"] = {["id"] = "b_n_nord_m_hair_01"},
			["M Hair 2"] = {["id"] = "b_n_nord_m_hair_02"},
			["M Hair 3"] = {["id"] = "b_n_nord_m_hair_03"},
			["M Hair 4"] = {["id"] = "b_n_nord_m_hair_04"},
			["M Hair 5"] = {["id"] = "b_n_nord_m_hair_05"},
			["M Hair 6"] = {["id"] = "b_n_nord_m_hair_06"},
			["M Hair 7"] = {["id"] = "b_n_nord_m_hair_07"},
			["M Hair 8"] = {["id"] = "b_n_nord_m_hair_08"}, -- bloodmoon
		},
		["orc"] = {
			["F Hair 1"] = {["id"] = "b_n_orc_f_hair_01"},
			["F Hair 2"] = {["id"] = "b_n_orc_f_hair_02"},
			["F Hair 3"] = {["id"] = "b_n_orc_f_hair_03"},
			["F Hair 4"] = {["id"] = "b_n_orc_f_hair_04"},
			["F Hair 5"] = {["id"] = "b_n_orc_f_hair_05"},
			["M Hair 1"] = {["id"] = "b_n_orc_m_hair_01"},
			["M Hair 2"] = {["id"] = "b_n_orc_m_hair_02"},
			["M Hair 3"] = {["id"] = "b_n_orc_m_hair_03"},
			["M Hair 4"] = {["id"] = "b_n_orc_m_hair_04"},
			["M Hair 5"] = {["id"] = "b_n_orc_m_hair_05"},
		},
		["redguard"] = {
			["F Spiky (Brown)"] = {["id"] = "b_n_redguard_f_hair_01"},
			["F Fade (Brown)"] = {["id"] = "b_n_redguard_f_hair_02"},
			["F Short Straight w/ Bangs (Red)"] = {["id"] = "b_n_redguard_f_hair_03"},
			["F Large Bun (Brown)"] = {["id"] = "b_n_redguard_f_hair_04"},
			["F Short Straight (Red)"] = {["id"] = "b_n_redguard_f_hair_05"},
			["Bald"] = {["id"] = "b_n_redguard_m_hair_00"},
			["M Receeding (Black)"] = {["id"] = "b_n_redguard_m_hair_01"},
			["M Tight Dreads (Black)"] = {["id"] = "b_n_redguard_m_hair_02"},
			["M Spiky (Brown)"] = {["id"] = "b_n_redguard_m_hair_03"},
			["M Fade (Black)"] = {["id"] = "b_n_redguard_m_hair_04"},
			["M Also Bald?"] = {["id"] = "b_n_redguard_m_hair_05"}, -- COME BACK TO THIS ONE
			["M Ponytail (Black)"] = {["id"] = "b_n_redguard_m_hair_06"},
		},
		["wood elf"] = {
			["F Hair 1"] = {["id"] = "b_n_wood elf_f_hair_01"},
			["F Hair 2"] = {["id"] = "b_n_wood elf_f_hair_02"},
			["F Hair 3"] = {["id"] = "b_n_wood elf_f_hair_03"},
			["F Hair 4"] = {["id"] = "b_n_wood elf_f_hair_04"},
			["F Hair 5"] = {["id"] = "b_n_wood elf_f_hair_05"},
			["M Hair 1"] = {["id"] = "b_n_wood elf_m_hair_01"},
			["M Hair 2"] = {["id"] = "b_n_wood elf_m_hair_02"},
			["M Hair 3"] = {["id"] = "b_n_wood elf_m_hair_03"},
			["M Hair 4"] = {["id"] = "b_n_wood elf_m_hair_04"},
			["M Hair 5"] = {["id"] = "b_n_wood elf_m_hair_05"},
			["M Hair 6"] = {["id"] = "b_n_wood elf_m_hair_06"},
		},
	},
	helmets = {
		["Chitin Helm"] = {["id"] = "chitin helm", ["cost"] = 19},
		["Indoril Helmet"] = {["id"] = "indoril helmet", ["cost"] = 3000},
		["Imperial Steel Helmet"] = {["id"] = "imperial helmet armor", ["cost"] = 70},
		["Netch Leather Helm"] = {["id"] = "netch_leather_helm", ["cost"] = 15},
		["Boiled Netch Leather Helm"] = {["id"] = "netch_leather_boiled_helm", ["cost"] = 17},
		["Nordic Fur Helm"] = {["id"] = "fur_helm", ["cost"] = 15},
		["Colovian Fur Helm"] = {["id"] = "fur_colovian_helm", ["cost"] = 25},
		["Telvanni Dust Adept Helm"] = {["id"] = "dust_adept_helm", ["cost"] = 30},
		["Telvanni Mole Crab Helm"] = {["id"] = "mole_crab_helm", ["cost"] = 19},
		["Telvanni Cephalopod Helm"] = {["id"] = "cephalopod_helm", ["cost"] = 50},
		["Imperial Chain Coif"] = {["id"] = "imperial_chain_coif_helm", ["cost"] = 35},
		["Chitin Mask Helm"] = {["id"] = "chitin_mask_helm", ["cost"] = 19},
		["Redoran Watchmans Helm"] = {["id"] = "chitin_watchman_helm", ["cost"] = 24},
		["Imperial Silver Helm"] = {["id"] = "silver_helm", ["cost"] = 120},
		["Nordic Iron Helm"] = {["id"] = "nordic_iron_helm", ["cost"] = 50},
		["Steel Helm"] = {["id"] = "steel_helm", ["cost"] = 60},
		["Native GahJulan Bonemold Helm"] = {["id"] = "bonemold_gahjulan_helm", ["cost"] = 165},
		["Native Chuzei Bonemold Helm"] = {["id"] = "bonemold_chuzei_helm", ["cost"] = 175},
		["Native ArmunAn Bonemold Helm"] = {["id"] = "bonemold_armunan_helm", ["cost"] = 150},
		["Morag Tong Helm"] = {["id"] = "morag_tong_helm", ["cost"] = 20},
		["Nordic Trollbone Helm"] = {["id"] = "trollbone_helm", ["cost"] = 65},
		["Imperial Dragonscale Helm"] = {["id"] = "dragonscale_helm", ["cost"] = 130},
		["Dwemer Helm"] = {["id"] = "dwemer_helm", ["cost"] = 450},
		["Orcish Helm"] = {["id"] = "orcish_helm", ["cost"] = 1200},
		["Dreugh Helm"] = {["id"] = "dreugh_helm", ["cost"] = 2250},
		["Redoran Master Helm"] = {["id"] = "redoran_master_helm", ["cost"] = 3000},
		["Glass Helm"] = {["id"] = "glass_helm", ["cost"] = 12000},
		["Ebony Closed Helm"] = {["id"] = "ebony_closed_helm", ["cost"] = 15000},
		["Daedric Face of Inspiration"] = {["id"] = "daedric_fountain_helm", ["cost"] = 13000},
		["Daedric Face of Terror"] = {["id"] = "daedric_terrifying_helm", ["cost"] = 14000},
		["Daedric Face of God"] = {["id"] = "daedric_god_helm", ["cost"] = 15000},
		["Imperial Templar Helmet"] = {["id"] = "templar_helmet_armor", ["cost"] = 75},
		["Iron Helmet"] = {["id"] = "iron_helmet", ["cost"] = 30},
		["Redoran Founders Helm"] = {["id"] = "bonemold_founders_helm", ["cost"] = 150},
		["Sarano Ebony Helm"] = {["id"] = "ebony_closed_helm_fghl", ["cost"] = 15000},
		["Velothian Helm"] = {["id"] = "velothian_helm", ["cost"] = 75},
		["Demon Helm"] = {["id"] = "demon helm", ["cost"] = 800},
		["Demon Mole Crab"] = {["id"] = "demon mole crab", ["cost"] = 900},
		["Demon Cephalopod"] = {["id"] = "demon cephalopod", ["cost"] = 1500},
		["Helm of Wounding"] = {["id"] = "helm of wounding", ["cost"] = 500},
		["Storm Helm"] = {["id"] = "storm helm", ["cost"] = 215},
		["Merisan Helm"] = {["id"] = "merisan helm", ["cost"] = 45},
		["Helm of Holy Fire"] = {["id"] = "helm of holy fire", ["cost"] = 7050},
		["Fiend Helm"] = {["id"] = "fiend helm", ["cost"] = 6000},
		["Devil Helm"] = {["id"] = "devil helm", ["cost"] = 1000},
		["Devil Mole Crab Helm"] = {["id"] = "devil mole crab helm", ["cost"] = 1200},
		["Devil Cephalopod Helm"] = {["id"] = "devil cephalopod helm", ["cost"] = 1700},
		["Native GahJulan Bonemold Helm"] = {["id"] = "bonemold_gahjulan_hhda", ["cost"] = 167},
		["Bonemold Helm"] = {["id"] = "bonemold_helm", ["cost"] = 150},
		["Masque of Clavicus Vile"] = {["id"] = "daedric_helm_clavicusvile", ["cost"] = 15000},
		["The Icecap"] = {["id"] = "icecap_unique", ["cost"] = 65},
		["Gothrens Cephalopod Helm"] = {["id"] = "cephalopod_helm_HTNK", ["cost"] = 50},
		["Gondoliers Helm"] = {["id"] = "gondolier_helm", ["cost"] = 10},
		["Indoril Helmet"] = {["id"] = "indoril helmet", ["cost"] = 3000}, -- tribunal
		["Nordic Fur Helm"] = {["id"] = "fur_helm", ["cost"] = 15}, -- tribunal
		["Steel Helm"] = {["id"] = "steel_helm", ["cost"] = 60}, -- tribunal
		["Nordic Trollbone Helm"] = {["id"] = "trollbone_helm", ["cost"] = 65}, -- tribunal
		["Iron Helmet"] = {["id"] = "iron_helmet", ["cost"] = 30}, -- tribunal
		["Bonemold Helm"] = {["id"] = "bonemold_helm", ["cost"] = 150}, -- tribunal
		["Imperial Silver Helm"] = {["id"] = "silver_helm_uvenim", ["cost"] = 120}, -- tribunal
		["Her Hands Helmet"] = {["id"] = "Indoril_Almalexia_helmet", ["cost"] = 12000}, -- tribunal
		["Royal Guard Helm"] = {["id"] = "Helsethguard_Helmet", ["cost"] = 550}, -- tribunal
		["Dark Brotherhood Helm"] = {["id"] = "DarkBrotherhood Helm", ["cost"] = 200}, -- tribunal
		["Adamantium Helm"] = {["id"] = "addamantium_helm", ["cost"] = 5000}, -- tribunal
		["Her Hands Helmet"] = {["id"] = "Indoril_MH_Guard_helmet", ["cost"] = 12000}, -- tribunal
		["Adamantium Helm"] = {["id"] = "adamantium_helm", ["cost"] = 5000}, -- tribunal
		["Steel Helm"] = {["id"] = "steel_helm", ["cost"] = 60}, -- bloodmoon
		["Imperial Templar Helmet"] = {["id"] = "templar_helmet_armor", ["cost"] = 75}, -- bloodmoon
		["Storm Helm"] = {["id"] = "storm helm", ["cost"] = 215}, -- bloodmoon
		["Bear Helmet"] = {["id"] = "BM Bear Helmet", ["cost"] = 40}, -- bloodmoon
		["Wolf Helmet"] = {["id"] = "BM Wolf Helmet", ["cost"] = 40}, -- bloodmoon
		["Ancient Steel Helm"] = {["id"] = "steel_helm_ancient", ["cost"] = 60}, -- bloodmoon
		["Red Colovian Fur Helm"] = {["id"] = "fur_colovian_helm_red", ["cost"] = 25}, -- bloodmoon
		["White Colovian Fur Helm"] = {["id"] = "fur_colovian_helm_white", ["cost"] = 25}, -- bloodmoon
		["Snow Wolf Helmet"] = {["id"] = "BM_wolf_helmet_snow", ["cost"] = 3500}, -- bloodmoon
		["Snow Bear Helmet"] = {["id"] = "BM_bear_helmet_snow", ["cost"] = 3500}, -- bloodmoon
		["Helm of Bear Scent"] = {["id"] = "BM Bear Helmet eddard", ["cost"] = 500}, -- bloodmoon
		["Nordic Mail Helmet"] = {["id"] = "BM_NordicMail_Helmet", ["cost"] = 1000}, -- bloodmoon
		["Ice Armor Helmet"] = {["id"] = "BM_Ice_Helmet", ["cost"] = 2000}, -- bloodmoon
		["Helmet of Bearkind"] = {["id"] = "BM Bear Helmet_ber", ["cost"] = 40}, -- bloodmoon
	},
	shirts = {
		["Common Shirt 1"] = {["id"] = "common_shirt_01", ["cost"] = 5},
		["Common Shirt 1 (Ahemmusa)"] = {["id"] = "common_shirt_01_a", ["cost"] = 5},
		["Common Shirt 1 (Erabenimsun)"] = {["id"] = "common_shirt_01_e", ["cost"] = 5},
		["Common Shirt 1 (Urshilaku)"] = {["id"] = "common_shirt_01_u", ["cost"] = 5},
		["Common Shirt 1 (Zainab)"] = {["id"] = "common_shirt_01_z", ["cost"] = 5},
		["Common Shirt 2"] = {["id"] = "common_shirt_02", ["cost"] = 5},
		["Common Shirt 2 (Hlaalu)"] = {["id"] = "common_shirt_02_h", ["cost"] = 5},
		["Common Shirt 2 (Hlaalu 2)"] = {["id"] = "common_shirt_02_hh", ["cost"] = 5},
		["Common Shirt 2 (Redoran)"] = {["id"] = "common_shirt_02_r", ["cost"] = 5},
		["Common Shirt 2 (Redoran 2)"] = {["id"] = "common_shirt_02_rr", ["cost"] = 5},
		["Common Shirt 2 (Telvanni)"] = {["id"] = "common_shirt_02_t", ["cost"] = 5},
		["Common Shirt 2 (Telvanni 2)"] = {["id"] = "common_shirt_02_tt", ["cost"] = 5},
		["Common Shirt 3 (a)"] = {["id"] = "common_shirt_03", ["cost"] = 5},
		["Common Shirt 3 (b)"] = {["id"] = "common_shirt_03_b", ["cost"] = 5},
		["Common Shirt 3 (c)"] = {["id"] = "common_shirt_03_c", ["cost"] = 5},
		["Common Shirt 4"] = {["id"] = "common_shirt_04", ["cost"] = 5},
		["Common Shirt 4 (a)"] = {["id"] = "common_shirt_04_a", ["cost"] = 5},
		["Common Shirt 4 (b)"] = {["id"] = "common_shirt_04_b", ["cost"] = 5},
		["Common Shirt 4 (c)"] = {["id"] = "common_shirt_04_c", ["cost"] = 5},
		["Common Shirt 5"] = {["id"] = "common_shirt_05", ["cost"] = 5},
		["Common Shirt 6"] = {["id"] = "common_shirt_06", ["cost"] = 5},
		["Common Shirt 7"] = {["id"] = "common_shirt_07", ["cost"] = 5},
		["Gondolier Shirt"] = {["id"] = "common_shirt_gondolier", ["cost"] = 5},
		["Expensive Shirt 1"] = {["id"] = "expensive_shirt_01", ["cost"] = 15},
		["Expensive Shirt 1 (Ahemmusa)"] = {["id"] = "expensive_shirt_01_a", ["cost"] = 15},
		["Expensive Shirt 1 (Erabenimsun)"] = {["id"] = "expensive_shirt_01_e", ["cost"] = 15},
		["Expensive Shirt 1 (Urshilaku)"] = {["id"] = "expensive_shirt_01_u", ["cost"] = 15},
		["Expensive Shirt 1 (Zainab)"] = {["id"] = "expensive_shirt_01_z", ["cost"] = 15},
		["Expensive Shirt 2"] = {["id"] = "expensive_shirt_02", ["cost"] = 15},
		["Expensive Shirt 3"] = {["id"] = "expensive_shirt_03", ["cost"] = 15},
		["Expensive Shirt (Mournhold)"] = {["id"] = "Expensive_shirt_Mournhold", ["cost"] = 15}, -- tribunal
		["Extravagant Shirt 1"] = {["id"] = "extravagant_shirt_01", ["cost"] = 300},
		["Extravagant Shirt 1 (Hlaalu)"] = {["id"] = "extravagant_shirt_01_h", ["cost"] = 300},
		["Extravagant Shirt 1 (Redoran)"] = {["id"] = "extravagant_shirt_01_r", ["cost"] = 300},
		["Extravagant Shirt 1 (Telvanni)"] = {["id"] = "extravagant_shirt_01_t", ["cost"] = 300},
		["Extravagant Shirt 2"] = {["id"] = "extravagant_shirt_02", ["cost"] = 300},
		["Nordic Shirt 1"] = {["id"] = "BM_Nordic01_shirt", ["cost"] = 5}, -- bloodmoon
		["Nordic Shirt 2"] = {["id"] = "BM_Nordic02_shirt", ["cost"] = 5}, -- bloodmoon
		["Wool Shirt 1"] = {["id"] = "BM_Wool01_shirt", ["cost"] = 5}, -- bloodmoon
		["Wool Shirt 2"] = {["id"] = "BM_Wool02_shirt", ["cost"] = 5}, -- bloodmoon
		
	},
	pants = {
		["Nordic Pants 1"] = {["id"] = "BM_Nordic01_pants", ["cost"] = 5}, -- bloodmoon
		["Nordic Pants 2"] = {["id"] = "BM_Nordic02_pants", ["cost"] = 5}, -- bloodmoon
		["Wool Pants 1"] = {["id"] = "BM_Wool01_pants", ["cost"] = 5}, -- bloodmoon
		["Wool Pants 2"] = {["id"] = "BM_Wool02_pants", ["cost"] = 5}, -- bloodmoon
		["Common Pants 1"] = {["id"] = "common_pants_01", ["cost"] = 5},
		["Common Pants 1 (Ahemmusa)"] = {["id"] = "common_pants_01_a", ["cost"] = 5},
		["Common Pants 1 (Erabenimsun)"] = {["id"] = "common_pants_01_e", ["cost"] = 5},
		["Common Pants 1 (Urshilaku)"] = {["id"] = "common_pants_01_u", ["cost"] = 5},
		["Common Pants 1 (Zainab)"] = {["id"] = "common_pants_01_z", ["cost"] = 5},
		["Common Pants 2"] = {["id"] = "common_pants_02", ["cost"] = 5},
		["Hentus Pants"] = {["id"] = "common_pants_02_hentus", ["cost"] = 5},
		["Common Pants 3 (a)"] = {["id"] = "common_pants_03", ["cost"] = 5},
		["Common Pants 3 (b)"] = {["id"] = "common_pants_03_b", ["cost"] = 5},
		["Common Pants 3 (c)"] = {["id"] = "common_pants_03_c", ["cost"] = 5},
		["Common Pants 4 (a)"] = {["id"] = "common_pants_04", ["cost"] = 5},
		["Common Pants 4 (a)"] = {["id"] = "common_pants_04_a", ["cost"] = 5},
		["Common Pants 4 (b)"] = {["id"] = "common_pants_04_b", ["cost"] = 5},
		["Common Pants 5"] = {["id"] = "common_pants_05", ["cost"] = 5},
		["Common Pants 6"] = {["id"] = "common_pants_06", ["cost"] = 5}, -- tribunal
		["Common Pants 7"] = {["id"] = "common_pants_07", ["cost"] = 5}, -- tribunal
		["Expensive Pants 1"] = {["id"] = "expensive_pants_01", ["cost"] = 15},
		["Expensive Pants 1 (Ahemmusa)"] = {["id"] = "expensive_pants_01_a", ["cost"] = 15},
		["Expensive Pants 1 (Erabenimsun)"] = {["id"] = "expensive_pants_01_e", ["cost"] = 15},
		["Expensive Pants 1 (Urshilaku)"] = {["id"] = "expensive_pants_01_u", ["cost"] = 15},
		["Expensive Pants 1 (Zainab)"] = {["id"] = "expensive_pants_01_z", ["cost"] = 15},
		["Expensive Pants 2"] = {["id"] = "expensive_pants_02", ["cost"] = 15},
		["Expensive Pants 3"] = {["id"] = "expensive_pants_03", ["cost"] = 15},
		["Expensive Pants (Mournhold)"] = {["id"] = "Expensive_pants_Mournhold", ["cost"] = 15}, -- tribunal
		["Exquisite Pants"] = {["id"] = "exquisite_pants_01", ["cost"] = 120},
		["Extravagant Pants 1"] = {["id"] = "extravagant_pants_01", ["cost"] = 60},
		["Extravagant Pants 2"] = {["id"] = "extravagant_pants_02", ["cost"] = 60},
		
	},
	robes = {
		["Nordic Robe"] = {["id"] = "BM_Nordic01_Robe", ["cost"] = 20}, -- bloodmoon
		["Wool Robe"] = {["id"] = "BM_Wool01_Robe", ["cost"] = 20}, -- bloodmoon
		["Common Robe 1"] = {["id"] = "common_robe_01", ["cost"] = 5},
		["Common Robe 2"] = {["id"] = "common_robe_02", ["cost"] = 5},
		["Common Robe 2 (Hlaalu)"] = {["id"] = "common_robe_02_h", ["cost"] = 5},
		["Common Robe 2 (Hlaalu 2)"] = {["id"] = "common_robe_02_hh", ["cost"] = 5},
		["Common Robe 2 (Redoran)"] = {["id"] = "common_robe_02_r", ["cost"] = 5},
		["Common Robe 2 (Redoran 2)"] = {["id"] = "common_robe_02_rr", ["cost"] = 5},
		["Common Robe 2 (Telvanni)"] = {["id"] = "common_robe_02_t", ["cost"] = 5},
		["Common Robe 2 (Telvanni 2)"] = {["id"] = "common_robe_02_tt", ["cost"] = 5},
		["Common Robe 3"] = {["id"] = "common_robe_03", ["cost"] = 5},
		["Common Robe 3 (a)"] = {["id"] = "common_robe_03_a", ["cost"] = 5},
		["Common Robe 3 (b)"] = {["id"] = "common_robe_03_b", ["cost"] = 5},
		["Common Robe 4"] = {["id"] = "common_robe_04", ["cost"] = 5},
		["Common Robe 5"] = {["id"] = "common_robe_05", ["cost"] = 5},
		["Common Robe 5 (a)"] = {["id"] = "common_robe_05_a", ["cost"] = 5},
		["Common Robe 5 (b)"] = {["id"] = "common_robe_05_b", ["cost"] = 5},
		["Common Robe 5 (c)"] = {["id"] = "common_robe_05_c", ["cost"] = 5},
		["Common Robe EOT"] = {["id"] = "common_robe_EOT", ["cost"] = 5}, -- tribunal
		["Expensive Robe 1"] = {["id"] = "expensive_robe_01", ["cost"] = 10},
		["Expensive Robe 2"] = {["id"] = "expensive_robe_02", ["cost"] = 10},
		["Expensive Robe 2 (a)"] = {["id"] = "expensive_robe_02_a", ["cost"] = 10},
		["Expensive Robe 3"] = {["id"] = "expensive_robe_03", ["cost"] = 10},
		["Exquisite Robe"] = {["id"] = "exquisite_robe_01", ["cost"] = 80},
		["Extravagant Robe 1"] = {["id"] = "extravagant_robe_01", ["cost"] = 40}, -- bloodmoon?
		["Extravagant Robe 1 (a)"] = {["id"] = "extravagant_robe_01_a", ["cost"] = 40},
		["Extravagant Robe 1 (b)"] = {["id"] = "extravagant_robe_01_b", ["cost"] = 40},
		["Extravagant Robe 1 (c)"] = {["id"] = "extravagant_robe_01_c", ["cost"] = 40},
		["Extravagant Robe 1 (Hlaalu)"] = {["id"] = "extravagant_robe_01_h", ["cost"] = 40},
		["Extravagant Robe 1 (Redoran)"] = {["id"] = "extravagant_robe_01_r", ["cost"] = 40},
		["Extravagant Robe 1 (Telvanni)"] = {["id"] = "extravagant_robe_01_t", ["cost"] = 40},
		["Extravagant Robe 2"] = {["id"] = "extravagant_robe_02", ["cost"] = 40},
	},
	skirts = {
		["Expensive Skirt (Mournhold)"] = {["id"] = "expensive_skirt_Mournhold", ["cost"] = 15}, -- tribunal
		["Common Skirt 1"] = {["id"] = "common_skirt_01", ["cost"] = 5},
		["Common Skirt 2"] = {["id"] = "common_skirt_02", ["cost"] = 5},
		["Common Skirt 3"] = {["id"] = "common_skirt_03", ["cost"] = 5},
		["Common Skirt 4 (Imperial)"] = {["id"] = "common_skirt_04", ["cost"] = 5},
		["Common Skirt 4"] = {["id"] = "common_skirt_04_c", ["cost"] = 5},
		["Common Skirt 5"] = {["id"] = "common_skirt_05", ["cost"] = 5},
		["Common Skirt 6"] = {["id"] = "common_skirt_06", ["cost"] = 5}, -- tribunal
		["Common Skirt 7"] = {["id"] = "common_skirt_07", ["cost"] = 5}, -- tribunal
		["Expensive Skirt 1"] = {["id"] = "expensive_skirt_01", ["cost"] = 15},
		["Expensive Skirt 2"] = {["id"] = "expensive_skirt_02", ["cost"] = 15},
		["Expensive Skirt 3"] = {["id"] = "expensive_skirt_03", ["cost"] = 15},
		["Expensive Skirt 4"] = {["id"] = "expensive_skirt_04", ["cost"] = 15}, -- tribunal
		["Exquisite Skirt"] = {["id"] = "exquisite_skirt_01", ["cost"] = 120},
		["Extravagant Skirt 1"] = {["id"] = "extravagant_skirt_01", ["cost"] = 60},
		["Extravagant Skirt 2"] = {["id"] = "extravagant_skirt_02", ["cost"] = 60},
		["Imperial Skirt"] = {["id"] = "imperial skirt_clothing", ["cost"] = 5},
		["Imperial Templar Skirt"] = {["id"] = "templar skirt obj", ["cost"] = 5}
	},
	rgloves = {
		["Common Right Glove"] = {["id"] = "common_glove_right_01", ["cost"] = 2},
		["Expensive Right Glove"] = {["id"] = "expensive_glove_right_01", ["cost"] = 10},
		["Extravagant Right Glove"] = {["id"] = "extravagant_glove_right_01", ["cost"] = 40},
		["Nordic Right Glove 1"] = {["id"] = "BM_Nordic01_gloveR", ["cost"] = 4}, -- bloodmoon
		["Nordic Right Glove 2"] = {["id"] = "BM_Nordic02_gloveR", ["cost"] = 4}, -- bloodmoon
		["Wool Right Glove 1"] = {["id"] = "BM_Wool01_gloveR", ["cost"] = 4}, -- bloodmoon
		["Wool Right Glove 2"] = {["id"] = "BM_Wool02_gloveR", ["cost"] = 4}, -- bloodmoon
		},
	lgloves = {
		["Common Left Glove"] = {["id"] = "common_glove_left_01", ["cost"] = 2},
		["Expensive Left Glove"] = {["id"] = "expensive_glove_left_01", ["cost"] = 10},
		["Extravagant Left Glove"] = {["id"] = "extravagant_glove_left_01", ["cost"] = 40},
		["Nordic Left Glove 1"] = {["id"] = "BM_Nordic01_gloveL", ["cost"] = 4}, -- bloodmoon
		["Nordic Left Glove 2"] = {["id"] = "BM_Nordic02_gloveL", ["cost"] = 4}, -- bloodmoon
		["Wool Left Glove 1"] = {["id"] = "BM_Wool01_gloveL", ["cost"] = 4}, -- bloodmoon
		["Wool Left Glove 2"] = {["id"] = "BM_Wool02_gloveL", ["cost"] = 4}, -- bloodmoon
	},
	shoes = {
		["Common Shoes 1"] = {["id"] = "common_shoes_01", ["cost"] = 2},
		["Common Shoes 2"] = {["id"] = "common_shoes_02", ["cost"] = 2},
		["Common Shoes 3"] = {["id"] = "common_shoes_03", ["cost"] = 2},
		["Common Shoes 4"] = {["id"] = "common_shoes_04", ["cost"] = 2},
		["Common Shoes 5"] = {["id"] = "common_shoes_05", ["cost"] = 2},
		["Expensive Shoes 1"] = {["id"] = "expensive_shoes_01", ["cost"] = 10},
		["Expensive Shoes 2"] = {["id"] = "expensive_shoes_02", ["cost"] = 10},
		["Expensive Shoes 3"] = {["id"] = "expensive_shoes_03", ["cost"] = 10},
		["Extravagant Shoes 1"] = {["id"] = "extravagant_shoes_01", ["cost"] = 40},
		["Extravagant Shoes 2"] = {["id"] = "extravagant_shoes_02", ["cost"] = 40},
		["Exquisite Shoes"] = {["id"] = "exquisite_shoes_01", ["cost"] = 80},
		["Expensive Shoes"] = {["id"] = "Expensive_shoes_Mournhold", ["cost"] = 10}, -- tribunal
		["Common Shoes 6"] = {["id"] = "common_shoes_06", ["cost"] = 2}, -- tribunal
		["Common Shoes 7"] = {["id"] = "common_shoes_07", ["cost"] = 2}, -- tribunal
		["Nordic Shoes 1"] = {["id"] = "BM_Nordic01_shoes", ["cost"] = 2}, -- bloodmoon
		["Nordic Shoes 2"] = {["id"] = "BM_Nordic02_shoes", ["cost"] = 2}, -- bloodmoon
		["Wool Shoes 1"] = {["id"] = "BM_Wool01_shoes", ["cost"] = 2}, -- bloodmoon
		["Wool Shoes 2"] = {["id"] = "BM_Wool02_shoes", ["cost"] = 2}, -- bloodmoon
	},
	cuirass = {
		["Chitin Cuirass"] = {["id"] = "chitin cuirass", ["cost"] = 45},
		["Imperial Templar Knight Cuirass"] = {["id"] = "templar_cuirass", ["cost"] = 175},
		["Indoril Cuirass"] = {["id"] = "indoril cuirass", ["cost"] = 7000},
		["Netch Leather Cuirass"] = {["id"] = "netch_leather_cuirass", ["cost"] = 35},
		["Boiled Netch Leather Cuirass"] = {["id"] = "netch_leather_boiled_cuirass", ["cost"] = 37},
		["Nordic Fur Cuirass"] = {["id"] = "fur_cuirass", ["cost"] = 35},
		["Nordic Bearskin Cuirass"] = {["id"] = "fur_bearskin_cuirass", ["cost"] = 35},
		["Imperial Studded Leather Cuiras"] = {["id"] = "imperial_studded_cuirass", ["cost"] = 65},
		["Imperial Chain Cuirass"] = {["id"] = "imperial_chain_cuirass", ["cost"] = 90},
		["Nordic Ringmail Cuirass"] = {["id"] = "nordic_ringmail_cuirass", ["cost"] = 80},
		["Imperial Newtscale Cuirass"] = {["id"] = "newtscale_cuirass", ["cost"] = 100},
		["Imperial Silver Cuirass"] = {["id"] = "silver_cuirass", ["cost"] = 280},
		["Dukes Guard Silver Cuirass"] = {["id"] = "silver_dukesguard_cuirass", ["cost"] = 350},
		["Nordic Iron Cuirass"] = {["id"] = "nordic_iron_cuirass", ["cost"] = 130},
		["Steel Cuirass"] = {["id"] = "steel_cuirass", ["cost"] = 150},
		["Iron Cuirass"] = {["id"] = "iron_cuirass", ["cost"] = 70},
		["GahJulan Bonemold Cuirass"] = {["id"] = "bonemold_gahjulan_cuirass", ["cost"] = 360},
		["ArmunAn Bonemold Cuirass"] = {["id"] = "bonemold_armunan_cuirass", ["cost"] = 350},
		["Nordic Trollbone Cuirass"] = {["id"] = "trollbone_cuirass", ["cost"] = 165},
		["Imperial Dragonscale Cuirass"] = {["id"] = "dragonscale_cuirass", ["cost"] = 340},
		["Dwemer Cuirass"] = {["id"] = "dwemer_cuirass", ["cost"] = 1050},
		["Orcish Cuirass"] = {["id"] = "orcish_cuirass", ["cost"] = 2800},
		["Dreugh Cuirass"] = {["id"] = "dreugh_cuirass", ["cost"] = 5250},
		["Glass Cuirass"] = {["id"] = "glass_cuirass", ["cost"] = 28000},
		["Ebony Cuirass"] = {["id"] = "ebony_cuirass", ["cost"] = 35000},
		["Daedric Cuirass"] = {["id"] = "daedric_cuirass", ["cost"] = 70000},
		["Imperial Steel Cuirass"] = {["id"] = "imperial cuirass_armor", ["cost"] = 150},
		["Merisan Cuirass"] = {["id"] = "merisan_cuirass", ["cost"] = 55},
		["The Chiding Cuirass"] = {["id"] = "the_chiding_cuirass", ["cost"] = 280},
		["Heart Wall"] = {["id"] = "heart wall", ["cost"] = 100},
		["Chest of Fire"] = {["id"] = "chest of fire", ["cost"] = 80},
		["Bonemold Cuirass"] = {["id"] = "bonemold_cuirass", ["cost"] = 350},
		["Lords Mail"] = {["id"] = "lords_cuirass_unique", ["cost"] = 190000},
		["Ebony Mail"] = {["id"] = "ebon_plate_cuirass_unique", ["cost"] = 120000},
		["Cuirass of the Saviors Hide"] = {["id"] = "cuirass_savior_unique", ["cost"] = 150000},
		["Dragonbone Cuirass"] = {["id"] = "dragonbone_cuirass_unique", ["cost"] = 180000},
		["Chitin Cuirass"] = {["id"] = "chitin cuirass", ["cost"] = 45}, -- tribunal
		["Netch Leather Cuirass"] = {["id"] = "netch_leather_cuirass", ["cost"] = 35}, -- tribunal
		["Imperial Studded Leather Cuiras"] = {["id"] = "imperial_studded_cuirass", ["cost"] = 65}, -- tribunal
		["Imperial Chain Cuirass"] = {["id"] = "imperial_chain_cuirass", ["cost"] = 90}, -- tribunal
		["Steel Cuirass"] = {["id"] = "steel_cuirass", ["cost"] = 150}, -- tribunal
		["Iron Cuirass"] = {["id"] = "iron_cuirass", ["cost"] = 70}, -- tribunal
		["Imperial Steel Cuirass"] = {["id"] = "imperial cuirass_armor", ["cost"] = 150}, -- tribunal
		["Bonemold Cuirass"] = {["id"] = "bonemold_cuirass", ["cost"] = 350}, -- tribunal
		["Sosceans Cuirass"] = {["id"] = "ebony_cuirass_soscean", ["cost"] = 32000}, -- tribunal
		["Her Hands Cuirass 1"] = {["id"] = "Indoril_Almalexia_Cuirass", ["cost"] = 50000}, -- tribunal
		["Royal Guard Cuirass"] = {["id"] = "Helsethguard_cuirass", ["cost"] = 8000}, -- tribunal
		["Dark Brotherhood Cuirass"] = {["id"] = "DarkBrotherhood Cuirass", ["cost"] = 1000}, -- tribunal
		["Adamantium Cuirass"] = {["id"] = "adamantium_cuirass", ["cost"] = 10000}, -- tribunal
		["Her Hands Cuirass 2"] = {["id"] = "Indoril_MH_Guard_Cuirass", ["cost"] = 50000}, -- tribunal
		["Steel Cuirass"] = {["id"] = "steel_cuirass", ["cost"] = 150}, -- bloodmoon
		["Bear Cuirass"] = {["id"] = "BM bear cuirass", ["cost"] = 150}, -- bloodmoon
		["Wolf Cuirass"] = {["id"] = "BM wolf cuirass", ["cost"] = 150}, -- bloodmoon
		["Ancient Steel Cuirass"] = {["id"] = "steel_cuirass_ancient", ["cost"] = 150}, -- bloodmoon
		["Snow Wolf Cuirass"] = {["id"] = "BM_wolf_cuirass_snow", ["cost"] = 10000}, -- bloodmoon
		["Snow Bear Cuirass"] = {["id"] = "BM_bear_cuirass_snow", ["cost"] = 10000}, -- bloodmoon
		["Nordic Mail Cuirass"] = {["id"] = "BM_NordicMail_cuirass", ["cost"] = 5000}, -- bloodmoon
		["Ice Armor Cuirass"] = {["id"] = "BM_Ice_cuirass", ["cost"] = 5000}, -- bloodmoon
	},
	lpauldrons = {
		["Chitin Left Pauldron"] = {["id"] = "chitin pauldron  left", ["cost"] = 16},
		["Indoril Left Pauldron"] = {["id"] = "indoril pauldron left", ["cost"] = 2400},
		["Imperial Templar Left Pauldron"] = {["id"] = "templar_pauldron_left", ["cost"] = 60},
		["Imperial Steel Left Pauldron"] = {["id"] = "imperial left pauldron", ["cost"] = 53},
		["Netch Leather Left Pauldron"] = {["id"] = "netch_leather_pauldron_left", ["cost"] = 12},
		["Nordic Fur Left Pauldron"] = {["id"] = "fur_pauldron_left", ["cost"] = 12},
		["Steel Left Pauldron"] = {["id"] = "steel_pauldron_left", ["cost"] = 48},
		["Iron Left Pauldron"] = {["id"] = "iron_pauldron_left", ["cost"] = 24},
		["GahJulan Bonemold L Pauldron"] = {["id"] = "bonemold_gahjulan_pauldron_l", ["cost"] = 140},
		["ArmunAn Bonemold L Pauldron"] = {["id"] = "bonemold_armunan_pauldron_l", ["cost"] = 120},
		["Dwemer Left Pauldron"] = {["id"] = "dwemer_pauldron_left", ["cost"] = 360},
		["Orcish Left Pauldron"] = {["id"] = "orcish_pauldron_left", ["cost"] = 960},
		["Ebony Left Pauldron"] = {["id"] = "ebony_pauldron_left", ["cost"] = 12000},
		["Daedric Left Pauldron"] = {["id"] = "daedric_pauldron_left", ["cost"] = 24000},
		["Imperial Chain Left Pauldron"] = {["id"] = "imperial_chain_pauldron_left", ["cost"] = 28},
		["Glass Left Pauldron"] = {["id"] = "glass_pauldron_left", ["cost"] = 9600},
		["Bonemold L Pauldron"] = {["id"] = "bonemold_pauldron_l", ["cost"] = 120},
		["Netch Leather Left Pauldron"] = {["id"] = "netch_leather_pauldron_left", ["cost"] = 12}, -- tribunal
		["Nordic Fur Left Pauldron"] = {["id"] = "fur_pauldron_left", ["cost"] = 12}, -- tribunal
		["Imperial Chain Left Pauldron"] = {["id"] = "imperial_chain_pauldron_left", ["cost"] = 28}, -- tribunal
		["Her Hands Left Pauldron 1"] = {["id"] = "Indoril_Almalexia_Pauldron_L", ["cost"] = 20000}, -- tribunal
		["Royal Guard Left Pauldron"] = {["id"] = "Helsethguard_pauldron_left", ["cost"] = 3000}, -- tribunal
		["Dark Brotherhood Left Pauldron"] = {["id"] = "DarkBrotherhood pauldron_L", ["cost"] = 500}, -- tribunal
		["Adamantium Left Pauldron"] = {["id"] = "adamantium_pauldron_left", ["cost"] = 800}, -- tribunal
		["Her Hands Left Pauldron 2"] = {["id"] = "Indoril_MH_Guard_Pauldron_L", ["cost"] = 20000}, -- tribunal
		["Bear Left Pauldron"] = {["id"] = "BM Bear left Pauldron", ["cost"] = 60}, -- bloodmoon
		["Wolf Left Pauldron"] = {["id"] = "BM Wolf Left Pauldron", ["cost"] = 60}, -- bloodmoon
		["Ancient Steel Left Pauldron"] = {["id"] = "steel_pauldron_left_ancient", ["cost"] = 48}, -- bloodmoon
		["Snow Wolf Left Pauldron"] = {["id"] = "BM_wolf_left_pauldron_snow", ["cost"] = 2000}, -- bloodmoon
		["Snow Bear Left Pauldron"] = {["id"] = "BM_Bear_left_Pauldron_snow", ["cost"] = 2000}, -- bloodmoon
		["Nordic Mail Left Pauldron"] = {["id"] = "BM_NordicMail_PauldronL", ["cost"] = 1000}, -- bloodmoon
		["Ice Armor Left Pauldron"] = {["id"] = "BM_Ice_PauldronL", ["cost"] = 12000}, -- bloodmoon
	},
	rpauldrons = {
		["Chitin Right Pauldron"] = {["id"] = "chitin pauldron  right", ["cost"] = 16},
		["Imperial Templar Right Pauldron"] = {["id"] = "templar_pauldron_right", ["cost"] = 60},
		["Indoril Right Pauldron"] = {["id"] = "indoril pauldron right", ["cost"] = 2400},
		["Imperial Steel Right Pauldron"] = {["id"] = "imperial right pauldron", ["cost"] = 53},
		["Steel Right Pauldron"] = {["id"] = "steel_pauldron_right", ["cost"] = 48},
		["Iron Right Pauldron"] = {["id"] = "iron_pauldron_right", ["cost"] = 24},
		["GahJulan Bonemold R Pauldron"] = {["id"] = "bonemold_gahjulan_pauldron_r", ["cost"] = 140},
		["ArmunAn Bonemold R Pauldron"] = {["id"] = "bonemold_armunan_pauldron_r", ["cost"] = 120},
		["Dwemer Right Pauldron"] = {["id"] = "dwemer_pauldron_right", ["cost"] = 360},
		["Orcish Right Pauldron"] = {["id"] = "orcish_pauldron_right", ["cost"] = 960},
		["Ebony Right Pauldron"] = {["id"] = "ebony_pauldron_right", ["cost"] = 12000},
		["Daedric Right Pauldron"] = {["id"] = "daedric_pauldron_right", ["cost"] = 24000},
		["Netch Leather Right Pauldron"] = {["id"] = "netch_leather_pauldron_right", ["cost"] = 12},
		["Nordic Fur Right Pauldron"] = {["id"] = "fur_pauldron_right", ["cost"] = 12},
		["Imperial Chain Right Pauldron"] = {["id"] = "imperial_chain_pauldron_right", ["cost"] = 28},
		["Glass Right Pauldron"] = {["id"] = "glass_pauldron_right", ["cost"] = 9600},
		["Bonemold R Pauldron"] = {["id"] = "bonemold_pauldron_r", ["cost"] = 120},
		["Netch Leather Right Pauldron"] = {["id"] = "netch_leather_pauldron_right", ["cost"] = 12}, -- tribunal
		["Nordic Fur Right Pauldron"] = {["id"] = "fur_pauldron_right", ["cost"] = 12}, -- tribunal
		["Imperial Chain Right Pauldron"] = {["id"] = "imperial_chain_pauldron_right", ["cost"] = 28}, -- tribunal
		["Bonemold R Pauldron"] = {["id"] = "bonemold_pauldron_r", ["cost"] = 120}, -- tribunal
		["Her Hands Right Pauldron 1"] = {["id"] = "Indoril_Almalexia_Pauldron_R", ["cost"] = 20000}, -- tribunal
		["Royal Guard Right Pauldron"] = {["id"] = "Helsethguard_pauldron_right", ["cost"] = 3000}, -- tribunal
		["Dark Brotherhood Right Pauldron"] = {["id"] = "DarkBrotherhood pauldron_R", ["cost"] = 500}, -- tribunal
		["Adamantium Right Pauldron"] = {["id"] = "adamantium_pauldron_right", ["cost"] = 800}, -- tribunal
		["Her Hands Right Pauldron 2"] = {["id"] = "Indoril_MH_Guard_Pauldron_R", ["cost"] = 20000}, -- tribunal

	},
	greaves = {
		["Chitin Greaves"] = {["id"] = "chitin greaves", ["cost"] = 29},
		["Netch Leather Greaves"] = {["id"] = "netch_leather_greaves", ["cost"] = 22},
		["Nordic Fur Greaves"] = {["id"] = "fur_greaves", ["cost"] = 22},
		["Imperial Steel Greaves"] = {["id"] = "imperial_greaves", ["cost"] = 98},
		["Imperial Templar Greaves"] = {["id"] = "templar_greaves", ["cost"] = 110},
		["Steel Greaves"] = {["id"] = "steel_greaves", ["cost"] = 88},
		["Iron Greaves"] = {["id"] = "iron_greaves", ["cost"] = 44},
		["Bonemold Greaves"] = {["id"] = "bonemold_greaves", ["cost"] = 220},
		["Dwemer Greaves"] = {["id"] = "dwemer_greaves", ["cost"] = 660},
		["Orcish Greaves"] = {["id"] = "orcish_greaves", ["cost"] = 1760},
		["Ebony Greaves"] = {["id"] = "ebony_greaves", ["cost"] = 22000},
		["Daedric Greaves"] = {["id"] = "daedric_greaves", ["cost"] = 44000},
		["Imperial Chain Greaves"] = {["id"] = "imperial_chain_greaves", ["cost"] = 50},
		["Glass Greaves"] = {["id"] = "glass_greaves", ["cost"] = 17600},
		["Chitin Greaves"] = {["id"] = "chitin greaves", ["cost"] = 29}, -- tribunal
		["Imperial Chain Greaves"] = {["id"] = "imperial_chain_greaves", ["cost"] = 50}, -- tribunal
		["Her Hands Greaves 1"] = {["id"] = "Indoril_Almalexia_Greaves", ["cost"] = 33000}, -- tribunal
		["Royal Guard Greaves"] = {["id"] = "Helsethguard_greaves", ["cost"] = 2000}, -- tribunal
		["Dark Brotherhood Greaves"] = {["id"] = "DarkBrotherhood greaves", ["cost"] = 100}, -- tribunal
		["Adamantium Greaves"] = {["id"] = "adamantium_greaves", ["cost"] = 10000}, -- tribunal
		["Her Hands Greaves 2"] = {["id"] = "Indoril_MH_Guard_Greaves", ["cost"] = 33000}, -- tribunal
		["Steel Greaves"] = {["id"] = "steel_greaves", ["cost"] = 88}, -- bloodmoon
		["Bear Greaves"] = {["id"] = "BM bear greaves", ["cost"] = 120}, -- bloodmoon
		["Wolf Greaves"] = {["id"] = "BM wolf greaves", ["cost"] = 120}, -- bloodmoon
		["Ancient Steel Greaves"] = {["id"] = "steel_greaves_ancient", ["cost"] = 88}, -- bloodmoon
		["Snow Wolf Greaves"] = {["id"] = "BM_wolf_greaves_snow", ["cost"] = 8500}, -- bloodmoon
		["Snow Bear Greaves"] = {["id"] = "BM_bear_greaves_snow", ["cost"] = 8500}, -- bloodmoon
		["Nordic Mail Greaves"] = {["id"] = "BM_NordicMail_greaves", ["cost"] = 2000}, -- bloodmoon
		["Ice Armor Greaves"] = {["id"] = "BM_Ice_greaves", ["cost"] = 1000}, -- bloodmoon
	},
	boots = {
		["Chitin Boots"] = {["id"] = "chitin boots", ["cost"] = 13},
		["Imperial Templar Boots"] = {["id"] = "templar boots", ["cost"] = 50},
		["Imperial Steel Boots"] = {["id"] = "imperial boots", ["cost"] = 50},
		["Indoril Boots"] = {["id"] = "indoril boots", ["cost"] = 2000},
		["Netch Leather Boots"] = {["id"] = "netch_leather_boots", ["cost"] = 10},
		["Nordic Fur Boots"] = {["id"] = "fur_boots", ["cost"] = 10},
		["Bonemold Boots"] = {["id"] = "bonemold_boots", ["cost"] = 100},
		["Dwemer Boots"] = {["id"] = "dwemer_boots", ["cost"] = 300},
		["Orcish Boots"] = {["id"] = "orcish_boots", ["cost"] = 800},
		["Ebony Boots"] = {["id"] = "ebony_boots", ["cost"] = 10000},
		["Daedric Boots"] = {["id"] = "daedric_boots", ["cost"] = 20000},
		["Steel Boots"] = {["id"] = "steel_boots", ["cost"] = 40},
		["Iron Boots"] = {["id"] = "iron boots", ["cost"] = 20},
		["Heavy Leather Boots"] = {["id"] = "heavy_leather_boots", ["cost"] = 100},
		["Glass Boots"] = {["id"] = "glass_boots", ["cost"] = 8000},
		["Chitin Boots"] = {["id"] = "chitin boots", ["cost"] = 13}, -- tribunal
		["Netch Leather Boots"] = {["id"] = "netch_leather_boots", ["cost"] = 10}, -- tribunal
		["Nordic Fur Boots"] = {["id"] = "fur_boots", ["cost"] = 10}, -- tribunal
		["Bonemold Boots"] = {["id"] = "bonemold_boots", ["cost"] = 100}, -- tribunal
		["Iron Boots"] = {["id"] = "iron boots", ["cost"] = 20}, -- tribunal
		["Her Hands Boots 1"] = {["id"] = "Indoril_Almalexia_boots", ["cost"] = 15000}, -- tribunal
		["Royal Guard Boots"] = {["id"] = "Helsethguard_boots", ["cost"] = 2500}, -- tribunal
		["Dark Brotherhood Boots"] = {["id"] = "DarkBrotherhood Boots", ["cost"] = 500}, -- tribunal
		["Adamantium Boots"] = {["id"] = "adamantium boots", ["cost"] = 7000}, -- tribunal
		["Her Hands Boots 2"] = {["id"] = "Indoril_MH_Guard_boots", ["cost"] = 15000}, -- tribunal
		["Steel Boots"] = {["id"] = "steel_boots", ["cost"] = 40}, -- bloodmoon
		["Bear Boots"] = {["id"] = "BM bear boots", ["cost"] = 50}, -- bloodmoon
		["Ancient Steel Boots"] = {["id"] = "steel_boots_ancient", ["cost"] = 40}, -- bloodmoon
		["Wolf Boots"] = {["id"] = "BM wolf boots", ["cost"] = 50}, -- bloodmoon
		["Snow Bear Boots"] = {["id"] = "BM_bear_boots_snow", ["cost"] = 5000}, -- bloodmoon
		["Snow Wolf Boots"] = {["id"] = "BM_wolf_boots_snow", ["cost"] = 5000}, -- bloodmoon
		["Nordic Mail Boots"] = {["id"] = "BM_NordicMail_Boots", ["cost"] = 5000}, -- bloodmoon
		["Ice Armor Boots"] = {["id"] = "BM_Ice_Boots", ["cost"] = 5000}, -- bloodmoon
	},
	lgauntlets = {
		["Chitin Left Gauntlet"] = {["id"] = "chitin guantlet  left", ["cost"] = 9},
		["Imperial Steel Left Gauntlet"] = {["id"] = "imperial left gauntlet", ["cost"] = 33},
		["Indoril Left Gauntlet"] = {["id"] = "indoril left gauntlet", ["cost"] = 1400},
		["Netch Leather Left Gauntlet"] = {["id"] = "netch_leather_gauntlet_left", ["cost"] = 7},
		["Steel Left Gauntlet"] = {["id"] = "steel_gauntlet_left", ["cost"] = 28},
		["Daedric Left Gauntlet"] = {["id"] = "daedric_gauntlet_left", ["cost"] = 14000},
		["Nordic Fur Left Gauntlet"] = {["id"] = "fur_gauntlet_left", ["cost"] = 7},
		["Iron Left Gauntlet"] = {["id"] = "iron_gauntlet_left", ["cost"] = 14},
		["Left Glove of the Horny Fist"] = {["id"] = "left_horny_fist_gauntlet", ["cost"] = 10},
		["Left Gauntlet of the Horny Fist"] = {["id"] = "left gauntlet of the horny fist", ["cost"] = 15},
		["Left Gauntlet of the Horny Fist"] = {["id"] = "gauntlet _horny_fist_l", ["cost"] = 40},
		["Boneweave Gauntlet"] = {["id"] = "boneweave gauntlet", ["cost"] = 130},
		["Left Gauntlet of Glory"] = {["id"] = "Gauntlet_of_Glory_left", ["cost"] = 1100},
		["Chitin Left Gauntlet"] = {["id"] = "chitin guantlet  left", ["cost"] = 9}, -- tribunal
		["Netch Leather Left Gauntlet"] = {["id"] = "netch_leather_gauntlet_left", ["cost"] = 7}, -- tribunal
		["Steel Left Gauntlet"] = {["id"] = "steel_gauntlet_left", ["cost"] = 28}, -- tribunal
		["Her Hands Left Gauntlet 1"] = {["id"] = "Indoril_Almalexia_gauntlet_L", ["cost"] = 13000}, -- tribunal
		["Royal Guard Left Gauntlet"] = {["id"] = "Helsethguard_gauntlet_left", ["cost"] = 2000}, -- tribunal
		["Dark Brotherhood Left Gauntlet"] = {["id"] = "DarkBrotherhood gauntlet_L", ["cost"] = 200}, -- tribunal
		["Her Hands Left Gauntlet 2"] = {["id"] = "Indoril_MH_Guard_gauntlet_L", ["cost"] = 13000}, -- tribunal
		["Bear Left Gauntlet"] = {["id"] = "bm bear left gauntlet", ["cost"] = 40}, -- bloodmoon
		["Wolf Left Gauntlet"] = {["id"] = "bm wolf left gauntlet", ["cost"] = 40}, -- bloodmoon
		["Ancient Steel Left Gauntlet"] = {["id"] = "steel_gauntlet_left_ancient", ["cost"] = 28}, -- bloodmoon
		["Snow Wolf Left Gauntlet"] = {["id"] = "BM_wolf_left_gauntlet_snow", ["cost"] = 2000}, -- bloodmoon
		["Snow Bear Left Gauntlet"] = {["id"] = "BM_bear_left_gauntlet_snow", ["cost"] = 2000}, -- bloodmoon
		["Nordic Mail Left Gauntlet"] = {["id"] = "BM_NordicMail_gauntletL", ["cost"] = 1000}, -- bloodmoon
		["Ice Armor Left Gauntlet"] = {["id"] = "BM_Ice_gauntletL", ["cost"] = 1000}, -- bloodmoon
	},
	rgauntlets = {
		["Chitin Right Gauntlet"] = {["id"] = "chitin guantlet  right", ["cost"] = 9},
		["Imperial Steel Right Gauntlet"] = {["id"] = "imperial right gauntlet", ["cost"] = 33},
		["Indoril Right Gauntlet"] = {["id"] = "indoril right gauntlet", ["cost"] = 1400},
		["Netch Leather Right Gauntlet"] = {["id"] = "netch_leather_gauntlet_right", ["cost"] = 7},
		["Steel Right Gauntlet"] = {["id"] = "steel_gauntlet_right", ["cost"] = 28},
		["Daedric Right Gauntlet"] = {["id"] = "daedric_gauntlet_right", ["cost"] = 14000},
		["Nordic Fur Right Gauntlet"] = {["id"] = "fur_gauntlet_right", ["cost"] = 7},
		["Iron Right Gauntlet"] = {["id"] = "iron_gauntlet_right", ["cost"] = 14},
		["Right Glove of the Horny Fist"] = {["id"] = "right horny fist gauntlet", ["cost"] = 10},
		["Right Gauntlet of Horny Fist"] = {["id"] = "right gauntlet of horny fist", ["cost"] = 15},
		["Right Gauntlet of Horny Fist"] = {["id"] = "gauntlet_horny_fist_r", ["cost"] = 40},
		["Bonedancer Gauntlet"] = {["id"] = "bonedancer gauntlet", ["cost"] = 130},
		["Right Gauntlet of Glory"] = {["id"] = "gauntlet_of_glory_right", ["cost"] = 1100},
		["Netch Leather Right Gauntlet"] = {["id"] = "netch_leather_gauntlet_right", ["cost"] = 7}, -- tribunal
		["Steel Right Gauntlet"] = {["id"] = "steel_gauntlet_right", ["cost"] = 28}, -- tribunal
		["Her Hands Right Gauntlet 1"] = {["id"] = "Indoril_Almalexia_gauntlet_R", ["cost"] = 13000}, -- tribunal
		["Royal Guard Right Gauntlet"] = {["id"] = "Helsethguard_gauntlet_right", ["cost"] = 2000}, -- tribunal
		["Dark Brotherhood Right Gauntlet"] = {["id"] = "DarkBrotherhood gauntlet_R", ["cost"] = 200}, -- tribunal
		["Her Hands Right Gauntlet 2"] = {["id"] = "Indoril_MH_Guard_gauntlet_R", ["cost"] = 13000}, -- tribunal
		["Bear Right Gauntlet"] = {["id"] = "BM bear right gauntlet", ["cost"] = 40}, -- bloodmoon
		["Wolf Right Gauntlet"] = {["id"] = "BM wolf right gauntlet", ["cost"] = 40}, -- bloodmoon
		["Ancient Steel Right Gauntlet"] = {["id"] = "steel_gauntlet_right_ancient", ["cost"] = 28}, -- bloodmoon
		["Snow Wolf Right Gauntlet"] = {["id"] = "BM_wolf_right_gauntlet_snow", ["cost"] = 2000}, -- bloodmoon
		["Snow Bear Right Gauntlet"] = {["id"] = "BM_bear_right_gauntlet_snow", ["cost"] = 2000}, -- bloodmoon
		["Nordic Mail Right Gauntlet"] = {["id"] = "BM_NordicMail_gauntletR", ["cost"] = 1000}, -- bloodmoon
		["Ice Armor Right Gauntlet"] = {["id"] = "BM_Ice_gauntletR", ["cost"] = 1000}, -- bloodmoon
	},
	lbracers = {
		["Imperial Templar Left Bracer"] = {["id"] = "templar bracer left", ["cost"] = 25},
		["Left Leather Bracer"] = {["id"] = "left leather bracer", ["cost"] = 5},
		["Cloth Left Bracer"] = {["id"] = "cloth bracer left", ["cost"] = 3},
		["Nordic Fur Left Bracer"] = {["id"] = "fur_bracer_left", ["cost"] = 5},
		["Slaves Left Bracer"] = {["id"] = "slave_bracer_left", ["cost"] = 5},
		["Iron Left Bracer"] = {["id"] = "iron_bracer_left", ["cost"] = 10},
		["Bonemold Left Bracer"] = {["id"] = "bonemold_bracer_left", ["cost"] = 50},
		["Dwemer Left Bracer"] = {["id"] = "dwemer_bracer_left", ["cost"] = 150},
		["Orcish Left Bracer"] = {["id"] = "orcish_bracer_left", ["cost"] = 400},
		["Ebony Left Bracer"] = {["id"] = "ebony_bracer_left", ["cost"] = 5000},
		["Bonemold Brace of Horny Fist"] = {["id"] = "lbonemold brace of horny fist", ["cost"] = 30},
		["Left Cloth Horny Fist Bracer"] = {["id"] = "left cloth horny fist bracer", ["cost"] = 7},
		["Ebony Left Bracer"] = {["id"] = "ebony_bracer_left_tgeb", ["cost"] = 5000},
		["Left Glass Bracer"] = {["id"] = "glass_bracer_left", ["cost"] = 4000},
		["Adamantium Left Bracer"] = {["id"] = "adamantium_bracer_left", ["cost"] = 1000}, -- tribunal
	},
	rbracers = {
		["Imperial Templar Right Bracer"] = {["id"] = "templar bracer right", ["cost"] = 25},
		["Right Leather Bracer"] = {["id"] = "right leather bracer", ["cost"] = 5},
		["Cloth Right Bracer"] = {["id"] = "cloth bracer right", ["cost"] = 3},
		["Nordic Fur Right Bracer"] = {["id"] = "fur_bracer_right", ["cost"] = 5},
		["Slaves Right Bracer"] = {["id"] = "slave_bracer_right", ["cost"] = 5},
		["Iron Right Bracer"] = {["id"] = "iron_bracer_right", ["cost"] = 10},
		["Bonemold Right Bracer"] = {["id"] = "bonemold_bracer_right", ["cost"] = 50},
		["Dwemer Right Bracer"] = {["id"] = "dwemer_bracer_right", ["cost"] = 150},
		["Ebony Right Bracer"] = {["id"] = "ebony_bracer_right", ["cost"] = 5000},
		["Orcish Right Bracer"] = {["id"] = "orcish_bracer_right", ["cost"] = 400},
		["Bonemold Bracer of Horny Fist"] = {["id"] = "rbonemold bracer of horny fist", ["cost"] = 30},
		["Right Cloth Horny Fist Bracer"] = {["id"] = "right cloth horny fist bracer", ["cost"] = 7},
		["Ebony Right Bracer"] = {["id"] = "ebony_bracer_right_tgeb", ["cost"] = 5000},
		["Right Glass Bracer"] = {["id"] = "glass_bracer_right", ["cost"] = 4000},
		["Adamantium Right Bracer"] = {["id"] = "adamantium_bracer_right", ["cost"] = 1000}, -- tribunal
	},
	shields = {
		["Indoril Shield"] = {["id"] = "indoril shield", ["cost"] = 2000},
		["Imperial Shield"] = {["id"] = "imperial shield", ["cost"] = 78},
		["Netch Leather Shield"] = {["id"] = "netch_leather_shield", ["cost"] = 17},
		["Netch Leather Tower Shield"] = {["id"] = "netch_leather_towershield", ["cost"] = 25},
		["Nordic Leather Shield"] = {["id"] = "nordic_leather_shield", ["cost"] = 25},
		["Chitin Shield"] = {["id"] = "chitin_shield", ["cost"] = 22},
		["Chitin Tower Shield"] = {["id"] = "chitin_towershield", ["cost"] = 32},
		["Steel Shield"] = {["id"] = "steel_shield", ["cost"] = 68},
		["Steel Tower Shield"] = {["id"] = "steel_towershield", ["cost"] = 100},
		["Iron Shield"] = {["id"] = "iron_shield", ["cost"] = 34},
		["Iron Tower Shield"] = {["id"] = "iron_towershield", ["cost"] = 50},
		["Bonemold Shield"] = {["id"] = "bonemold_shield", ["cost"] = 170},
		["Bonemold Tower Shield"] = {["id"] = "bonemold_towershield", ["cost"] = 250},
		["Nordic Trollbone Shield"] = {["id"] = "trollbone_shield", ["cost"] = 78},
		["Dragonscale Tower Shield"] = {["id"] = "dragonscale_towershield", ["cost"] = 230},
		["Dwemer Shield"] = {["id"] = "dwemer_shield", ["cost"] = 510},
		["Orcish Tower Shield"] = {["id"] = "orcish_towershield", ["cost"] = 2000},
		["Dreugh Shield"] = {["id"] = "dreugh_shield", ["cost"] = 2550},
		["Glass Shield"] = {["id"] = "glass_shield", ["cost"] = 13600},
		["Glass Tower Shield"] = {["id"] = "glass_towershield", ["cost"] = 20000},
		["Ebony Shield"] = {["id"] = "ebony_shield", ["cost"] = 17000},
		["Ebony Tower Shield"] = {["id"] = "ebony_towershield", ["cost"] = 25000},
		["Daedric Shield"] = {["id"] = "daedric_shield", ["cost"] = 34000},
		["Daedric Tower Shield"] = {["id"] = "daedric_towershield", ["cost"] = 50000},
		["Hlaalu Guard Shield"] = {["id"] = "bonemold_tshield_hlaaluguard", ["cost"] = 250},
		["Redoran Guard Shield"] = {["id"] = "bonemold_tshield_redoranguard", ["cost"] = 250},
		["Telvanni Guard Shield"] = {["id"] = "bonemold_tshield_telvanniguard", ["cost"] = 250},
		["Shield of Light"] = {["id"] = "shield_of_light", ["cost"] = 85},
		["Feather Shield"] = {["id"] = "feather_shield", ["cost"] = 120},
		["Velothis Shield"] = {["id"] = "velothis_shield", ["cost"] = 85},
		["Holy Shield"] = {["id"] = "holy_shield", ["cost"] = 220},
		["Blessed Shield"] = {["id"] = "blessed_shield", ["cost"] = 130},
		["Veloths Tower Shield"] = {["id"] = "veloths_tower_shield", ["cost"] = 150},
		["Holy Tower Shield"] = {["id"] = "holy_tower_shield", ["cost"] = 3100},
		["Shield of Wounds"] = {["id"] = "shield of wounds", ["cost"] = 620},
		["Velothian Shield"] = {["id"] = "velothian shield", ["cost"] = 85},
		["Succour of Indoril"] = {["id"] = "succour of indoril", ["cost"] = 4200},
		["Spirit of Indoril"] = {["id"] = "spirit of indoril", ["cost"] = 5400},
		["Saints Shield"] = {["id"] = "saints shield", ["cost"] = 15000},
		["Azuras Servant"] = {["id"] = "azuras servant", ["cost"] = 30000},
		["Bonemold Shield"] = {["id"] = "bonemold_shield", ["cost"] = 170}, -- tribunal
		["Dreugh Shield"] = {["id"] = "dreugh_shield", ["cost"] = 2550}, -- tribunal
		["Goblin Buckler"] = {["id"] = "goblin_shield", ["cost"] = 1000}, -- tribunal
		["Her Hands Shield 1"] = {["id"] = "Indoril_Almalexia_shield", ["cost"] = 2500}, -- tribunal
		["Her Hands Shield 2"] = {["id"] = "Indoril_MH_Guard_shield", ["cost"] = 2500}, -- tribunal
		["Nordic Leather Shield"] = {["id"] = "nordic_leather_shield", ["cost"] = 25},
		["Steel Shield"] = {["id"] = "steel_shield", ["cost"] = 68}, -- bloodmoon
		["Blessed Shield"] = {["id"] = "blessed_shield", ["cost"] = 130}, -- bloodmoon
		["Saints Shield"] = {["id"] = "saints shield", ["cost"] = 15000}, -- bloodmoon
		["Ancient Steel Tower Shield"] = {["id"] = "steel_towershield_ancient", ["cost"] = 100}, -- bloodmoon
		["Riekling Shield"] = {["id"] = "BM_Ice minion_Shield1", ["cost"] = 50}, -- bloodmoon
		["Nordic Mail Shield"] = {["id"] = "BM_NordicMail_Shield", ["cost"] = 1000}, -- bloodmoon
		["Ice Shield"] = {["id"] = "BM_Ice_Shield", ["cost"] = 1000}, -- bloodmoon
		["Wolf Shield"] = {["id"] = "BM wolf shield", ["cost"] = 100}, -- bloodmoon
		["Bear Shield"] = {["id"] = "BM bear shield", ["cost"] = 100}, -- bloodmoon
	},
	misc = {
		["Blank Paper"] = {["id"] = "sc_paper plain", ["cost"] = 1}
	},
}

return defaultConfig
