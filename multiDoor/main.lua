-- multiDoor for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- WORK IN PROGRESS, NOT FOR PUBLIC USE
-- system for the creation and management of instanced housing (cells with shared doors)
-- under GPLv3

multiDoor = {}

multiDoor.scriptName = "multiDoor"
multiDoor.defaultConfig = require("custom.multiDoor.defaultConfig")
multiDoor.defaultData = multiDoor.defaultConfig
multiDoor.data = DataManager.loadData(multiDoor.scriptName, multiDoor.defaultData)
local cellList = multiDoor.data.doors

table.insert(guiHelper.names, "multiDoor_DoorActivate")
table.insert(guiHelper.names, "multiDoor_CellList") -- for adding a GUI later
table.insert(guiHelper.names, "multiDoor_DoorList")
table.insert(guiHelper.names, "multiDoor_DoorSettings")

guiHelper.ID = tableHelper.enum(guiHelper.names)

local function ClearTable(pid)
	multiDoor[pid] = {}
	multiDoor[pid].activatedDoor = ""
end

local function ShowDoorActivateMenu(eventStatus,pid,uniqueIndex)
	local cellDescription = tes3mp.GetCell(pid)
	local message = color.Orange .. "[multiDoor]"
	local options = ""
	
	tes3mp.LogMessage(2, uniqueIndex)
	tes3mp.LogMessage(2, cellDescription)
	
	for instanceName,instance in ipairs(cellList[cellDescription][uniqueIndex]) do
		options = options .. instance.doorName .. ";"
	end
	
	tes3mp.CustomMessageBox(pid, guiHelper.ID.multiDoor_DoorActivate, message, options .. "Close")
end

local function OnObjectActivateValidator(eventStatus,pid,cellDescription,objects,players)
	for cellID,_ in pairs(cellList) do
		if cellDescription == cellID then
			for doorUniqueIndex,indexTable in pairs(cellList[cellID]) do
				for _, object in pairs(objects) do
					if object.uniqueIndex == doorUniqueIndex then
						ClearTable(pid)
						multiDoor[pid].activatedDoor = doorUniqueIndex -- create global uniqueIndex of door
						ShowDoorActivateMenu(eventStatus,pid,doorUniqueIndex)
						return customEventHooks.makeEventStatus(false, nil) -- cancel activate action
					end
				end
			end
		end
	end
end

local function CheckGUI(newStatus,pid,idGui,data)
	local cellDescription = tes3mp.GetCell(pid)
	if idGui == guiHelper.ID.multiDoor_DoorActivate then
		if tonumber(data) == 0 then -- passthrough action (if used on door, door will work as usual)
			tes3mp.LogMessage(2, multiDoor[pid].activatedDoor)
			logicHandler.ActivateObjectForPlayer(pid,cellDescription,multiDoor[pid].activatedDoor)
			tes3mp.LogMessage(2, "[multiDoor] selected 0")
			ClearTable(pid)
		elseif tonumber(data) == 1 then -- teleport to custom cell
			tes3mp.LogMessage(2, "[multiDoor] selected 1")
			local desiredCellTable = cellList[cellDescription][multiDoor[pid].activatedDoor][(tonumber(data)+1)]
			if desiredCellTable.destCell ~= nil then
				tes3mp.SetCell(pid, desiredCellTable.destCell)
				tes3mp.SetPos(pid, desiredCellTable.destPos[1], desiredCellTable.destPos[2], desiredCellTable.destPos[3], desiredCellTable.destPos[4], desiredCellTable.destPos[5], desiredCellTable.destPos[6])
				tes3mp.SendCell(pid)
				tes3mp.SendPos(pid)
			else
				tes3mp.LogMessage(2, "[multiDoor] CELL DESTINATION INVALID")
			end
			return customEventHooks.makeEventStatus(false, nil)
		else
			tes3mp.LogMessage(2, "[multiDoor] selected invalid")
			return customEventHooks.makeEventStatus(false, nil)
		end
	end
end
local function ChatListener(pid, cmd)
	local cellDescription = tes3mp.GetCell(pid)
	if Players[pid]:IsAdmin() and cmd[1] == "multidoor" then
		if cmd[2] == nil then
			Players[pid]:Message(color.Gray .. "How to make a multiDoor:\n" .. 
			"1. /multidoor uniqueIndex create\n" .. 
			"2. /multidoor uniqueIndex defDoorName NameForFirstOption\n" .. 
			"3. /multidoor uniqueIndex doorName NameForSecondOption\n" ..
			"4. /multidoor uniqueIndex destCell WhereToTeleportPlayer\n" ..
			"5. /multidoor uniqueIndex destPos PosX PosY PosZ RotX RotY RotZ\n" .. color.Default)
		else
			tes3mp.LogMessage(2, cellDescription)
			if multiDoor.data.doors[cellDescription] == nil then
				multiDoor.data.doors[cellDescription] = {}
			end
			local uniqueIndex = cmd[2]
			if cmd[3] == "create" then
				if multiDoor.data.doors[cellDescription][uniqueIndex] == nil then
					multiDoor.data.doors[cellDescription][uniqueIndex] = {}
					multiDoor.data.doors[cellDescription][uniqueIndex][1] = {}
					multiDoor.data.doors[cellDescription][uniqueIndex][1].doorName = "NameForFirstOption"
					multiDoor.data.doors[cellDescription][uniqueIndex][2] = {}
					multiDoor.data.doors[cellDescription][uniqueIndex][2].doorName = "NameForSecondOption"
					multiDoor.data.doors[cellDescription][uniqueIndex][2].destCell = "0,0"
					multiDoor.data.doors[cellDescription][uniqueIndex][2].destPos = {0,0,0,0,0,0}
					DataManager.saveData("multiDoor",multiDoor.data)
				else
					Players[pid]:Message(color.Gray .. "That's already a multidoor! Skip step 1.\n" .. color.Default)
				end
			elseif cmd[3] == "defDoorName" then
				multiDoor.data.doors[cellDescription][uniqueIndex][1].doorName = table.concat(cmd, " ", 4)
				DataManager.saveData("multiDoor",multiDoor.data)
			elseif cmd[3] == "doorName" then
				multiDoor.data.doors[cellDescription][uniqueIndex][2].doorName = table.concat(cmd, " ", 4)
				DataManager.saveData("multiDoor",multiDoor.data)
			elseif cmd[3] == "destCell" then
				multiDoor.data.doors[cellDescription][uniqueIndex][2].destCell = table.concat(cmd, " ", 4)
				DataManager.saveData("multiDoor",multiDoor.data)
			elseif cmd[3] == "destPos" then
				multiDoor.data.doors[cellDescription][uniqueIndex][2].destPos = {
				tonumber(cmd[4]),
				tonumber(cmd[5]),
				tonumber(cmd[6]),
				tonumber(cmd[7]),
				tonumber(cmd[8]),
				tonumber(cmd[9])}
				DataManager.saveData("multiDoor",multiDoor.data)
			end
		end
	end
end

customEventHooks.registerValidator("OnObjectActivate", OnObjectActivateValidator)
customEventHooks.registerValidator("OnGUIAction", CheckGUI)
customEventHooks.registerValidator("OnPlayerDisconnect", ClearTable)
customEventHooks.registerHandler("OnPlayerAuthentified", ClearTable)
customCommandHooks.registerCommand("multidoor", ChatListener)
