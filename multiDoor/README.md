## multiDoor (for 0.7-alpha)
-	A script to give doors admin-defined multiple destinations
-	Player is presented a GUI of multiple destinations a door can lead to
-	Under GPLv3

### Installation
-	Install [DataManager](https://github.com/tes3mp-scripts/DataManager) by uramer
-	Add main.lua and defaultConfig.lua to scripts/custom/multiDoor/
-	Add to customScripts.lua somewhere under DataManager's line:
		`require("custom.multiDoor.main")`

### Usage
-

### To-do
-	Add custom cell record support (when it exists)