-- defaultConfig for multiDoor. Contains example
local defaultConfig = {
	doors = {
		["-3, -2"] = { -- balmora
			["32812-0"] = { -- balmora, eight plates door
				[1] = { -- default action
					["doorName"] = "Balmora, Eight Plates"
				},
				[2] = { -- custom cell
					["doorName"] = "Balmora, Potion Shop",
					["destCell"] = "Mournhold",
					["destPos"] = {-21,2,-122,0,0,0}
				}
			}
		}
	}
}

return defaultConfig
