-- itemCatalog for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- a framework that creates a large list of refIds and their item names, for other scripts to refer to
-- requires espParser and DataFramework
-- under GPLv3

-- to-do: add custom record scraping

itemCatalog = {}
itemCatalog.scriptName = "itemCatalog"
itemCatalog.defaultData = {}
itemCatalog.data = DataManager.loadData(itemCatalog.scriptName, itemCatalog.defaultData)

function itemCatalog.GetItemName(requestId) -- this is probably horribly inefficient
	for refId,name in pairs(itemCatalog.data) do
		if requestId == refId then
			return name
		end
	end
end

local function GenericParser(header)
	for _,record in pairs(espParser.getAllRecords(header)) do
		local refId = ""
		local name = ""
		for _, subrecord in pairs(record.subRecords) do
			if subrecord.name == "NAME" then
				refId = struct.unpack("s", subrecord.data)
			elseif subrecord.name == "FNAM" then
				name = struct.unpack("s", subrecord.data)
			end
		end
		if refId ~= nil and name ~= nil and refId ~= "" and name ~= "" then
			itemCatalog.data[refId] = name
		end
	end
end

local function OnServerPostInitHandler()
	tes3mp.LogMessage(2, "[itemCatalog] Initialized")
	itemCatalog.data = itemCatalog.defaultData -- clear list on every server start so it stays updated
	DataManager.saveData("itemCatalog",itemCatalog.data)
	tes3mp.LogMessage(2, "[itemCatalog] Parsing armor")
	GenericParser("ARMO")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing clothing")
	GenericParser("CLOT")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing armor")
	GenericParser("REPA")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing activators")
	GenericParser("ACTI")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing apparatus'")
	GenericParser("APPA")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing lockpicking")
	GenericParser("LOCK")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing probes")
	GenericParser("PROB")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing ingredients")
	GenericParser("INGR")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing books")
	GenericParser("BOOK")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing alchemy")
	GenericParser("ALCH")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing lights")
	GenericParser("LIGH")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing containers")
	GenericParser("CONT")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing weapons")
	GenericParser("WEAP")
	tes3mp.LogMessage(2, "[itemCatalog] Parsing misc")
	GenericParser("MISC")
	tes3mp.LogMessage(2, "[itemCatalog] Finished")
	DataManager.saveData("itemCatalog",itemCatalog.data)
end

local function ChatListener(pid,cmd) -- for testing
	if cmd[1] == "itemcatalog" and cmd[2] ~= nil then
		local refId = itemCatalog.GetItemName(cmd[2])
		if refId ~= nil then
			Players[pid]:Message(color.Grey .. refId .. "\n" .. color.Default)
		else
			Players[pid]:Message(color.Grey .. "not found\n" .. color.Default)
		end
	end
end

customEventHooks.registerHandler("OnServerPostInit", OnServerPostInitHandler)
customCommandHooks.registerCommand("itemcatalog", ChatListener)
