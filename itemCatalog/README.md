## itemCatalog (for 0.7-alpha)
-	A framework that creates a large list of refId's and their item names, for other scripts to refer to
-	Requires [espParser](https://github.com/JakobCh/tes3mp_scripts/blob/master/espParser/) and [DataManager](https://github.com/tes3mp-scripts/DataManager)
-	Under GPLv3

### Installation
-	Install [DataManager](https://github.com/tes3mp-scripts/DataManager) by uramer
-	Install [espParser](https://github.com/JakobCh/tes3mp_scripts/blob/master/espParser/) by Jakob
-	Add main.lua to scripts/custom/itemCatlaog
-	Add to customScripts.lua somewhere under DataManager, espParser, and struct's line:
		`require("custom.itemCatalog.main")`

### Commands
-   **/itemcatalog refidhere -** Get the name of an item from its refId

### To-do
-	Add custom record support
