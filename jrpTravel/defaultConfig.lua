local defaultConfig = {
	options = {
		["mage_guild_portal"] = false, -- not reimplemented yet
		["mage_guild_guide"] = true,
		["telvanni_portal"] = true
	},
	mage_guild_guides = {"masalinie merian", "erranil", "emelia duronia", "iniel", "flacassia fauseius", 
"tr_m0_ohmonir", "tr_m2_mjara", "trtr_androthen_guide", "trtr_almalexia_guide", "trtr_almalexia_hub", 
"trtr_baan_malur_guide", "trtr_baan_malur_hub", "tr_m1_hessei_lig", "trtr_bosmora_guide", "trtr_cormar_guide",
"tr_m1_garath", "tr_m1_arlie_drest", "tr_m2_simpremus comnor", "trtr_kartur_guide", "trtr_kogotel_guide",
"trtr_kragen_mar_guide", "trtr_narsis_guide", "trtr_narsis_hub", "tr_m3_elvilde"},
	destinations = {
		mageguild = {
			["Akamora"] = {
				cell = "Akamora, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {-177,-422,-382,0,0,0}
			},
			["Ald-ruhn"] = {
				cell = "Ald-ruhn, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {2560,-516,-354,0,0,0}
			},
			["Almalexia (PREVIEW)"] = {
				cell = "Almalexia, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {-153.75,585.7,-190,0,0,0}
			},
			["Andothren (PREVIEW)"] = {
				cell = "TR Andothren, Guild of Mages [i4-125]",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {7865,4339,15136.7,0,0,0}
			},
			["Bal Oyra"] = {
				cell = "Bal Oyra, Keep: Mages Guild Relay",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {-752.5,-126.5,-53.5,0,0,0}
			},
			["Balmora"] = {
				cell = "Balmora, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {-709,-1001,-730,0,0,0}
			},
			["Bosmora"] = {
				cell = "33, -30",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {274264.5,-239721,1446.59,0,0,0}
			},
			["Caldera"] = {
				cell = "Caldera, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {558,285,500,0,0,0}
			},
			["Cormar"] = {
				cell = "Cormar, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {-12.5,389.7,-207,0,0,0}
			},
			["Firewatch"] = {
				cell = "Firewatch Palace, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {3893.3,4048.65,10065,0,0,0}
			},
			["Helnim"] = {
				cell = "Helnim, Helnim Hall: Mages Guild Relay",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {4581.5,4351.7,15057,0,0,0}
			},
			["Kartur"] = {
				cell = "Kartur, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {3955,5958,12443,0,0,0}
			},
			["Kogotel"] = {
				cell = "Kogotel, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {30.5,-200,333,0,0,0}
			},
			["Kragen Mar"] = {
				cell = "Kragen Mar, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {40,-182,272.3,0,0,0}
			},
			["Narsis"] = {
				cell = "Narsis, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {50,173,-54.5,0,0,0}
			},
			["Old Ebonheart"] = {
				cell = "Old Ebonheart, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {4673.5,3405.5,15233,0,0,0}
			},
			["Sadrith Mora"] = {
				cell = "Sadrith Mora, Wolverine Hall: Mage's Guild",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {-39,194.65,77,0,0,0}
			},
			["Vivec"] = {
				cell = "Vivec, Guild of Mages",
				portal_pos = {0,0,0,0,0,0},
				result_pos = {4.8,1343,-479,0,0,0}
			}
		},
		telvanni = {
			["Alt Bosara"] = {
				cell = "37, -4",
				portal_pos = {303900.5,-30938.7,1425,0,0,0},
				result_pos = {303928,-30826.7,1309,0,0,0}
			},
			["Gah Sadrith"] = {
				cell = "42, 13",
				portal_pos = {345026,112402,945,0,0,0},
				result_pos = {344947,112430,832,0,0,0}
			},
			["Port Telvannis"] = {
				cell = "41, 16",
				portal_pos = {343223.6,133355,330,0,0,0},
				result_pos = {343248,133292,244,0,0,0}
			},
			["Ranyon-ruhn"] = {
				cell = "27, 12",
				portal_pos = {229251,102292,3290,0,0,0},
				result_pos = {229416,102264,3144,0,0,0}
				
			},
			["Sadrith Mora"] = {
				cell = "17, 4",
				portal_pos = {141968.3,36997.3,312,0,0,0},
				result_pos = {142089.7,37058.2,221,0,0,0}
			},
			["Tel Aruhn"] = {
				cell = "Tel Aruhn, Tower Entry",
				portal_pos = {4042,3954.6,13690,0,0,0},
				result_pos = {4036.5,3850,13625,0,0,0}
			},
			["Tel Branora"] = {
				cell = "15, -13",
				portal_pos = {123118,-102183.5,748.4,0,0,0},
				result_pos = {122969.6,-102160.7,644.7,0,0,0}
			},
			["Tel Fyr"] = {
				cell = "15, 2",
				portal_pos = {124238,18764.9,435.7,0,0,0},
				result_pos = {124259.9,18867.8,372.3,0,0,0}
			},
			["Tel Mora"] = {
				cell = "13, 14",
				portal_pos = {109828.5,116152.5,410,0,0,0},
				result_pos = {109835,116067,276.7,0,0,0}
			},
			["Tel Mothrivra"] = {
				cell = "33, 2",
				portal_pos = {277844.1,22277.76,1270,0,0,0},
				result_pos = {277917,22347.6,1174,0,0,0}
			},
			["Tel Oren"] = {
				cell = "33, 2",
				portal_pos = {175623,93453.5,211,0,0,0},
				result_pos = {175672,93442,121,0,0,0}
			},
			["Tel Ouada"] = {
				cell = "25, 18",
				portal_pos = {212253.3,148253.7,383,0,0,0},
				result_pos = {212386.9,148248.8,261,0,0,0}
			},
			--[[["Tel Sudia"] = { -- Example of custom destination, city mod
				cell = "38, 5",
				portal_pos = {312348.125,42872.29,184.9,0,0,0},
				result_pos = {312441,42952.8,120.4,0,0,0}
			},--]]
			["Tel Vos"] = {
				cell = "10, 14",
				portal_pos = {88830,118311,3312.6,0,0,0},
				result_pos = {88902,118241,3159,0,0,0}
			}
		}
	}
}

return defaultConfig
