-- jrpTravel for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- menu to teleport between any mages guild/telvanni tower, no extra trips necessary
-- this mod needs Tamriel Rebuilt or other guild travel destinations to be of any value
-- under GPLv3

local jrpTravel = {}

jrpTravel.scriptName = "jrpTravel"
jrpTravel.defaultConfig = require("custom.jrpTravel.defaultConfig")
jrpTravel.config = DataManager.loadConfiguration(jrpTravel.scriptName, jrpTravel.defaultConfig)

table.insert(guiHelper.names, "jrpTravel_talkMage")
table.insert(guiHelper.names, "jrpTravel_travelMage")
table.insert(guiHelper.names, "jrpTravel_travelTelvanni")
guiHelper.ID = tableHelper.enum(guiHelper.names)

-- initialize portal records. thanks uramer!
function jrpTravel.createRecord(refId,name,model)
    local recordStore = RecordStores["miscellaneous"]
    recordStore.data.permanentRecords[refId] = {
        name = name,
        model = model,
        script = "noPickUp"
    }
	recordStore:Save()
end

-- mage guild guide hijack (if enabled)
function jrpTravel.showTalkMageGUI(eventStatus,pid)
	local message = color.Cyan .. "jrpTravel\n"
	tes3mp.CustomMessageBox(pid, guiHelper.ID.jrpTravel_talkMage, message, "Travel;Talk;Cancel")
end

-- Default travel GUI
function jrpTravel.showTravelMageGUI(eventStatus,pid)
	local message = color.Cyan.. "- Mage Guild Travel Nexus -\n" ..color.White.. "Where will you travel?\n" ..color.Yellow.. "10 gold each\n"
	mageguildDestinations = {}
	for destinationName,_ in pairs(jrpTravel.config.destinations.mageguild) do
		table.insert(mageguildDestinations,destinationName)
		table.sort(mageguildDestinations)
	end
	local printedResults = table.concat(mageguildDestinations,";")

	tes3mp.CustomMessageBox(pid, guiHelper.ID.jrpTravel_travelMage, message, "CLOSE;" .. printedResults)
end

function jrpTravel.showTravelTelvanniGUI(eventStatus,pid)
	local message = color.Purple.. "- Telvanni Travel Nexus -\n" ..color.White.. "Where will you travel?\n" ..color.Yellow.. "10 gold each\n"
	telvanniDestinations = {}
	for destinationName,_ in pairs(jrpTravel.config.destinations.telvanni) do
		table.insert(telvanniDestinations,destinationName)
		table.sort(telvanniDestinations)
	end
	local printedResults = table.concat(telvanniDestinations,";")

	tes3mp.CustomMessageBox(pid, guiHelper.ID.jrpTravel_travelTelvanni, message, "CLOSE;" .. printedResults)
end

-- ty atkana <3
local function getPlayerGold(pid)
	local goldLoc = inventoryHelper.getItemIndex(Players[pid].data.inventory, "gold_001", -1)
	
	if goldLoc then
		return Players[pid].data.inventory[goldLoc].count
	else
		return 0
	end
end

-- again ty atkana <3
local function removeGold(pid, amount)
	local goldLoc = inventoryHelper.getItemIndex(Players[pid].data.inventory, "gold_001", -1)	
	Players[pid].data.inventory[goldLoc].count = Players[pid].data.inventory[goldLoc].count - amount
	
	local itemref = {refId = "gold_001", count = amount, charge = -1}			
	Players[pid]:Save()
	Players[pid]:LoadItemChanges({itemref}, enumerations.inventory.REMOVE)			
end

function jrpTravel.teleportPlayer(eventStatus,pid,desiredcell,location)
	-- just remove 10 gold on teleport for now
	local pgold = getPlayerGold(pid)
	if pgold < 10 then
		tes3mp.MessageBox(pid, -1, "You can't afford to travel.")
	else
		removeGold(pid,10)
		tes3mp.SetCell(pid, desiredcell)
		tes3mp.SetPos(pid, location[1], location[2], location[3])
		tes3mp.SendCell(pid)
		tes3mp.SendPos(pid)
		tes3mp.MessageBox(pid, -1, "10 gold has been removed from your inventory.")
	end
end

function jrpTravel.checkGUI(newStatus,pid,idGui,data)
	local cellDescription 
	if idGui == guiHelper.ID.jrpTravel_talkMage then
		if tonumber(data) == 0 then -- travel
			jrpTravel.showTravelMageGUI(eventStatus,pid)
			return customEventHooks.makeEventStatus(false, nil)
		elseif tonumber(data) == 1 then -- run vanilla activate
			local cellDescription = tes3mp.GetCell(pid)
			logicHandler.ActivateObjectForPlayer(pid,cellDescription,objectUniqueIndex)
		end
	elseif idGui == guiHelper.ID.jrpTravel_travelMage then
		if tonumber(data) ~= 0 then
			local desiredcell = jrpTravel.config.destinations.mageguild[mageguildDestinations[tonumber(data)]].cell
			local location = jrpTravel.config.destinations.mageguild[mageguildDestinations[tonumber(data)]].result_pos
			jrpTravel.teleportPlayer(eventStatus,pid,desiredcell,location)
		end
	elseif idGui == guiHelper.ID.jrpTravel_travelTelvanni then
		if tonumber(data) ~= 0 then
			local desiredcell = jrpTravel.config.destinations.telvanni[telvanniDestinations[tonumber(data)]].cell
			local location = jrpTravel.config.destinations.telvanni[telvanniDestinations[tonumber(data)]].result_pos
			jrpTravel.teleportPlayer(eventStatus,pid,desiredcell,location)
		end
	end
end

function jrpTravel.OnObjectActivateValidator(eventStatus, pid, cellDescription, objects, players)
    for _, object in pairs(objects) do
		for _, guildguide in pairs(jrpTravel.config.mage_guild_guides) do
			if object.refId == guildguide then
				objectUniqueIndex = object.uniqueIndex
				jrpTravel.showTalkMageGUI(eventStatus,pid)
				return customEventHooks.makeEventStatus(false, nil)
			end
		end
        if object.refId == "jrptravel_mageguild" then
			jrpTravel.showTravelMageGUI(eventStatus,pid)
			return customEventHooks.makeEventStatus(false, nil)
		elseif object.refId == "jrptravel_telvanni" then
			jrpTravel.showTravelTelvanniGUI(eventStatus,pid)
			return customEventHooks.makeEventStatus(false, nil)
		end
    end
end

function jrpTravel.OnServerInit()
	if jrpTravel.config.options.mage_guild_portal == true then
		jrpTravel.createRecord("jrptravel_mageguild","Mages Guild Travel Nexus","w\\magic_target_conjure.NIF")
	end
	if jrpTravel.config.options.telvanni_portal == true then
		jrpTravel.createRecord("jrptravel_telvanni","Telvanni Travel Nexus","w\\magic_target_alt.NIF")
	end
end

local function OnServerPostInit()
	CellInserter.data = DataManager.loadData("CellInserter",CellInserter.defaultData)
	local insert = CellInserter.data["insert"]
	for menuname,destination in pairs(jrpTravel.config.destinations.telvanni) do
		if destination.portal_pos ~= nil then
			CellInserter.addToData(destination.cell,"jrptravel_telvanni",destination.portal_pos,1)
		end
	end
	DataManager.saveData("CellInserter",CellInserter.data)
end

customEventHooks.registerValidator("OnGUIAction", jrpTravel.checkGUI)
customEventHooks.registerValidator("OnObjectActivate", jrpTravel.OnObjectActivateValidator)
customEventHooks.registerHandler("OnServerInit", jrpTravel.OnServerInit)
customEventHooks.registerHandler("OnServerPostInit", OnServerPostInit)
