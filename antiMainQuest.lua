-- antiMainQuest for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- under GPLv3

local antiMainQuest = {}
antiMainQuest.blockDagothUrDoor = true

table.insert(guiHelper.names, "antiMainQuest_dagothBlock")
guiHelper.ID = tableHelper.enum(guiHelper.names)

local function OnObjectActivateV(eventStatus,pid,cellDescription,objects,players)
	for _, object in pairs(objects) do
		if antiMainQuest.blockDagothUrDoor == true then
			if cellDescription == "2, 8" and object.refId == "door_dwrv_load00" then
				tes3mp.CustomMessageBox(pid, guiHelper.ID.antiMainQuest_dagothBlock, "Horrid images fill your mind as your hand grips the doorhandle. Your prior confidence is stolen from you as you yank your hand back. Whatever is in here, you are not ready for it.", "Close")
				tes3mp.LogMessage(2,"[antiMainQuest] " .. Players[pid].name .. " was denied entry into Dagoth Ur through the entry door.")
				return customEventHooks.makeEventStatus(false, nil)
			end
		end
	end
end

customEventHooks.registerValidator("OnObjectActivate", OnObjectActivateV)
