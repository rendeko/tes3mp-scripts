-- jrpWerewolf for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- fake being a werewolf for roleplay purposes
-- thanks to Algurt for WerewolfRobe
-- under GPLv3

local jrpWerewolf = {}
jrpWerewolf.disguisePlayer = false -- disguises the player to other players (uses debug werewolf creature)

local function ClearTable(pid)
	jrpWerewolf[pid] = {}
	jrpWerewolf[pid].pastRace = ""
	jrpWerewolf[pid].pastHead = ""
	jrpWerewolf[pid].pastHair = ""
end

local function RevertPlayer(eventStatus, pid)
	Players[pid].data.equipment[11] = nil
	Players[pid].data.character.race = jrpWerewolf[pid].pastRace
	tes3mp.SetRace(pid, jrpWerewolf[pid].pastRace)
	Players[pid].data.character.head = jrpWerewolf[pid].pastHead
	tes3mp.SetHead(pid, jrpWerewolf[pid].pastHead)
	Players[pid].data.character.hair = jrpWerewolf[pid].pastHair
	tes3mp.SetHair(pid, jrpWerewolf[pid].pastHair)
	tes3mp.SetResetStats(pid, false)
	tes3mp.SendBaseInfo(pid)
	Players[pid]:LoadInventory()
	Players[pid]:LoadEquipment()
	Players[pid]:LoadShapeshift()
	Players[pid]:Save()
		
	if jrpWerewolf.disguisePlayer == true then
		Players[pid].data.shapeshift.creatureRefId = "nothing"
		tes3mp.SetCreatureRefId(pid, "nothing")
		tes3mp.SendShapeshift(pid)
	end
	
	Players[pid]:LoadShapeshift()
	Players[pid]:Save()
	ClearTable(pid)
end

local function BecomeWerewolf(eventStatus,pid)
	if Players[pid].data.equipment[11] == nil then
		local structuredItem = {enchantmentCharge = -1, refId = "werewolfrobe", count = 1, charge = -1}
		table.insert(Players[pid].data.equipment, 11, structuredItem)
	else
		Players[pid].data.equipment[11].refId = "werewolfrobe"
		Players[pid].data.equipment[11].count = 1
		Players[pid].data.equipment[11].charge = -1
		Players[pid].data.equipment[11].enchantmentCharge = -1
	end
	
	Players[pid]:LoadInventory()
	Players[pid]:LoadEquipment()

	jrpWerewolf[pid].pastRace = Players[pid].data.character.race
	jrpWerewolf[pid].pastHead = Players[pid].data.character.head
	jrpWerewolf[pid].pastHair = Players[pid].data.character.hair

	Players[pid].data.character.race = "argonian"
	tes3mp.SetRace(pid, "argonian")
	Players[pid].data.character.head = "WerewolfHead"
	tes3mp.SetHead(pid, "WerewolfHead")
	Players[pid].data.character.hair = "WerewolfHair"
	tes3mp.SetHair(pid, "WerewolfHair")
	tes3mp.SetResetStats(pid, false)
	tes3mp.SendBaseInfo(pid)
	
	if jrpWerewolf.disguisePlayer == true then
		Players[pid].data.shapeshift.creatureRefId = "BM_werewolf_default"
		tes3mp.SetCreatureRefId(pid, "BM_werewolf_default")
		tes3mp.SendShapeshift(pid)
	end
	
	Players[pid]:LoadShapeshift()
	Players[pid]:Save()
end

local function SetWerewolf(eventStatus,pid)
	if jrpWerewolf[pid] == nil then
		ClearTable(pid)
	end

	if Players[pid].data.equipment[11] == nil and jrpWerewolf[pid].pastRace == "" then -- no robe, not transformed
		BecomeWerewolf(eventStatus,pid)
		tes3mp.SendMessage(pid, color.IndianRed .. "You've become a werewolf.\n" .. color.Default,false)
		tes3mp.LogMessage(2, "[jrpWerewolf] " .. Players[pid].name .. " used /werewolf and turned into a werewolf.")
	elseif Players[pid].data.equipment[11] ~= nil and jrpWerewolf[pid].pastRace ~= "" then -- robe, transformed
			RevertPlayer(eventStatus, pid)
			tes3mp.SendMessage(pid, color.SlateGray .. "You awaken from your trance.\n" .. color.Default,false)
			tes3mp.LogMessage(2, "[jrpWerewolf] " .. Players[pid].name .. " used /werewolf and reverted to their past form.")
	elseif Players[pid].data.equipment[11] ~= nil and jrpWerewolf[pid].pastRace == "" then -- robe, not transformed
		tes3mp.SendMessage(pid, color.SlateGray .. "Please remove your robe and try again.\n" .. color.Default,false)
		tes3mp.LogMessage(2, "[jrpWerewolf] " .. Players[pid].name .. " used /werewolf but was already wearing a robe and rejected.")
	end
end

local function RevertCheck(eventStatus,pid)
	if jrpWerewolf[pid] == nil then
		ClearTable(pid)
	end
	if jrpWerewolf[pid].pastRace ~= "" then
		RevertPlayer(eventStatus, pid)
	end
end

local function ChatListener(pid,cmd)
	if cmd[1] == "werewolf" then
		SetWerewolf(eventStatus,pid)
	end
end

customCommandHooks.registerCommand("werewolf", ChatListener)
customEventHooks.registerHandler("OnPlayerAuthentified", ClearTable)
customEventHooks.registerValidator("OnPlayerDisconnect", RevertCheck)
customEventHooks.registerHandler("OnPlayerDeath", RevertCheck)
